<?php
session_start();
/*
 * Copyright 2018 Media Web Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$config = require_once dirname(__FILE__) . '/app/config.php';
date_default_timezone_set($config['timezone']);
define('PATH', __DIR__);
define('DSN', 'mysql:host='.$config['db']['host'].';dbname='.$config['db']['name'].';charset=utf8mb4');
define('USR', $config['db']['user']);
define('PWD', $config['db']['password']);

include dirname(__FILE__) . '/app/init.php';

// BEGIN APP
$loader = new Twig_Loader_Filesystem('views/');
$twig = new Twig_Environment($loader, $config['twig']);
$twig->addExtension(new AppExtension());
$twig->addGlobal('months', $months);
