<?php

require_once __DIR__ . ("/app.php");

$table = 'users';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$data['tab_target'] = ( !empty($_GET['target']) ) ? $_GET['target'] : 'profile';

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {

    if ( isset($_POST['update-profil']) ) {

        $nama = $_POST['nama'];
        $email = $_POST['email'];

        updateMany('users', array('nama' => $nama), array('email' => $email), array('id', '=', $app->user['id']));

				$app->add_flash('success', 'Profil berhasil diperbarui...');
        $app->redirect('profile.php?target=profile');

    } 
		elseif ( isset($_POST['ganti-password']) ) {
			
        $password = ( !empty($_POST['password']) ) ? $_POST['password'] : '';
				$password = $app->encode_pw($password);

				$result = $app
					->updateOrInsert( 
						$table, 
						array(
							'id'	=> $app->user['id'],
							'password' => $password
						),
						$app->user['id']
					);

				$app->add_flash('success', 'Password berhasil diganti...');
				$app->redirect( 'profile.php?target=ganti-password' );

    }

}

if ( $aksi == "edit" ) {

  if ( !empty($_GET['id']) ) {
    $data['request'] = $app->getUser($_GET['id']);
  }

  $data['levels'] = $app->getMaster('user_level');
  

} else {
  
  $level = ( isset($_GET['level']) ) ? $_GET['level'] : '';
  $data['users'] = $app->getListUser($level);
  

}

try {

  echo $twig->render( 'default/profil_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {

  die ('ERROR: ' . $e->getMessage());
  
}