<?php

require_once __DIR__ . ("/app.php");

$table = 'pembayaran';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$id = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;

$data['status_pembayaran'] = $app->getMaster('status_pembayaran');
$data['jenis_pendapatan'] = $app->getMaster('jenis_pendapatan');
$data['years'] = array();
for( $i = date('Y'); $i >= 2000; $i= $i-1 ) {
	$data['years'][] = $i;
}
$data['months'] = array(
	'01'	=> 'Januari',
	'02'	=> 'Februari',
	'03'	=> 'Maret',
	'04'	=> 'April',
	'05'	=> 'Mei',
	'06'	=> 'Juni',
	'07'	=> 'Juli',
	'08'	=> 'Agustus',
	'09'	=> 'September',
	'10'	=> 'Oktober',
	'11'	=> 'November',
	'12'	=> 'Desember'
);

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
	
		if( isset( $_POST['submit-filter'] ) ) {
			
			$params = array(
				'pagination'	=> false,
				'order_by'		=> 'tanggal',
				'order'				=> 'DESC',
				'query'				=> $_REQUEST
			);
			if( !empty( $_POST['id_siswa'] ) ) {
				$params['id_siswa'] = $_POST['id_siswa'];
			}
			if( !empty( $_POST['id_jenis_pendapatan'] ) ) {
				$params['id_jenis_pendapatan'] = (int)$_POST['id_jenis_pendapatan'];
			}
			
			$data['data'] = $app->getListPembayaran($params);
			
		} elseif ( isset( $_POST['submit-pembayaran'] ) ) {

			$app->debug($_POST);
			
			if ( count($_POST['jumlah']) > 0 ) {
				$id_jenis_pendapatan = $_POST['id_jenis_pendapatan'];
				$jumlah = $_POST['jumlah'];
				for ($i=0; $i < count($jumlah); $i++) { 
					if ( !empty($jumlah[$i]) && !empty($id_jenis_pendapatan[$i]) ) {

						$id_transaksi = $app->getIdTransaksi('pembayaran', $id);

						$params = array(
							'tanggal'      		=> $_POST['tanggal'],
							'id_kas'      		=> $_POST['id_kas'],
							'id_siswa'      	=> $_POST['id_siswa'],
							'id_jenis_pendapatan' => $id_jenis_pendapatan[$i],
							'bulan_ke' 			=> $_POST['bulan_ke'],
							'jumlah'         	=> $jumlah[$i],
							'status' 			=> $_POST['status'],
							'keterangan'      	=> $_POST['keterangan'],
							'id_transaksi'		=> $id_transaksi,
						);
					
						if( empty($id) || $id == 0 ) {
							$params['tahun'] = date('Y');
						}

						$result = $app
							->updateOrInsert( 
								$table, 
								$params,
								$id
							);

						$jenis_pembayaran = $app->getMasterDetails($params['id_jenis_pendapatan']);
						$ket_transaksi = ucfirst($jenis_pembayaran['nama']);
						$ket_transaksi = str_replace('Pembayaran', '', $ket_transaksi);
						$ket_transaksi = 'Pembayaran ' . trim($ket_transaksi);
					
						$transaksi_params = array(
							'id_kas'	=> $params['id_kas'],
							'tanggal'	=> $params['tanggal'],
							'jenis'     => 'credit',
							'jumlah' 	=> $params['jumlah'],
							'ket'      	=> $ket_transaksi,
						);

						$result = $app
							->updateOrInsert( 
								'transaksi', 
								$transaksi_params,
								$id_transaksi
							);

						$app->add_flash('success', 'Data berhasil disimpan...');
						$app->redirect( 'entry.php' );

					}
				}
			}

		}
		else {
			
				$id_transaksi = $app->getIdTransaksi('pembayaran', $id);

				$params = array(
					'tanggal'      				=> $_POST['tanggal'],
					'id_kas'      				=> $_POST['id_kas'],
					'id_siswa'      			=> $_POST['id_siswa'],
					'id_jenis_pendapatan' => $_POST['id_jenis_pendapatan'],
					'bulan_ke' 						=> $_POST['bulan_ke'],
					'jumlah'         			=> $_POST['jumlah'],
					'status' 							=> $_POST['status'],
					'keterangan'      		=> $_POST['keterangan'],
					'id_transaksi'				=> $id_transaksi,
				);
			
				if( empty($id) || $id == 0 ) {
					$params['tahun'] = date('Y');
				}

				$result = $app
					->updateOrInsert( 
						$table, 
						$params,
						$id
					);

				$jenis_pembayaran = $app->getMasterDetails($params['id_jenis_pendapatan']);
				$ket_transaksi = ucfirst($jenis_pembayaran['nama']);
				$ket_transaksi = str_replace('Pembayaran', '', $ket_transaksi);
				$ket_transaksi = 'Pembayaran ' . trim($ket_transaksi);
			
				$transaksi_params = array(
					'id_kas'		=> $params['id_kas'],
					'tanggal'		=> $params['tanggal'],
					'jenis'     => 'credit',
					'jumlah' 		=> $params['jumlah'],
					'ket'      	=> $ket_transaksi,
				);

				$result = $app
					->updateOrInsert( 
						'transaksi', 
						$transaksi_params,
						$id_transaksi
					);

				$app->add_flash('success', 'Data berhasil disimpan...');
				$app->redirect( 'entry.php' );
			
		}

} else {
	
	if ( $aksi == "edit" && isset($_GET['id']) ) {

		$data['request'] = $app->getPembayaran($_GET['id']);
		$data['akun_kas'] = $app->getListKas(); 

	} elseif ( $aksi == 'new' ) {
		$html = '';
		$html .= '<tr><td><select name="id_jenis_pendapatan[]" class="form-control"><option value=""></option>';
		foreach ($data['jenis_pendapatan'] as $jp) {
			$html .= '<option value="'.$jp['id'].'">'.$jp['nama'].'</option>';
		}
		$html .= '</select></td>';
		$html .= '<td width="280">';
		$html .= '<input type="text" class="form-control" name="jumlah[]" value=""></td>';
		$html .= '</tr>';

		$data['html_pembayaran'] = $html;
		$data['akun_kas'] = $app->getListKas(); 
	} 
	else {

		$params = array(
			'id_siswa'		=> ( isset($_GET['siswa']) ) ? $_GET['siswa'] : 0,
			'pagination'	=> true,
			'offset'			=> 0,
			'page'				=> ( isset($_GET['page']) ) ? $_GET['page'] : 1,
			'order_by'		=> 'id',
			'order'				=> 'ASC',
		);
		$data['data'] = $app->getListPembayaran( $params );

	}

}

// $app->debug($data['data']);

try {
  
	echo $app->load( 'default/pembayaran_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}