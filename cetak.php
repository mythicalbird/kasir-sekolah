<?php

require_once __DIR__ . ("/app.php");

$table = 'pembayaran';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$id = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;
$jenis_pendapatan = $app->getMaster('jenis_pendapatan');

if ( $aksi == "pembayaran_siswa" ) {

	$bulan_mulai = $_GET['mulai'];
	$bulan_akhir = $_GET['akhir'];
	$bulan_arr = array();
	if( !empty($bulan_mulai) || !empty($bulan_akhir) ) {
		if( $bulan_akhir > $bulan_mulai ) {
			for( $i=$bulan_mulai; $i <= $bulan_akhir; $i++  ) {
				$bulan_arr[] = $months[$i];
			}
		} else {
			if( !empty($bulan_mulai) ) {
				$bulan_arr = array($months[$bulan_mulai]);
			} else {
				$bulan_arr = array($months[$bulan_akhir]);
			}
		}
	} else {
		for( $i=1; $i <= count($months); $i++  ) {
			$bulan_arr[] = $months[$i];
		}
	}

	$id_jenis_pendapatan = $_GET['id_jenis_pendapatan'];
	if( !empty($id_jenis_pendapatan) ) {
		$id_jenis_pendapatan = explode(",", $id_jenis_pendapatan);
	} else {
		$id_jenis_pendapatan = array();
		foreach($jenis_pendapatan as $jp) {
			$id_jenis_pendapatan[] = $jp['id'];
		}
	}
	
	$params = array(
		'id_siswa'						=> $_GET['id_siswa'],
		'tahun'								=> $_GET['tahun'],
		'bulan_mulai' 				=> $bulan_mulai,
		'bulan_akhir' 				=> $bulan_akhir,
		'bulan'								=> $bulan_arr,
		'id_jenis_pendapatan'	=> ( !empty($id_jenis_pendapatan) ) ? $id_jenis_pendapatan : array()
	);

	$report = new Report( $params );
	$result = $report->pembayaran();

	$data['data'] = $result;
	
// 	$app->debug($result);
  

} 
elseif ( $aksi == "tagihan_siswa" ) {

	$params = array(
		'query'								=> $_REQUEST,
		'id_kelas'						=> $_GET['id_kelas'],
		'tahun'								=> $_GET['tahun'],
		'id_jenis_pendapatan'	=> $_GET['id_jenis_pendapatan']
	);
	$report = new Report( $params );
	$result = $report->tagihan();

	$data['data'] = $result;
	
// 	$app->debug($result);
  

} 
elseif ( $aksi == "lapkas" ) {

	$params = array(
		'query'								=> $_REQUEST,
		'jenis_laporan'				=> $_GET['jenis_laporan'],
		'jenis_transaksi'			=> ( !empty($_GET['jenis_transaksi']) ) ? $_GET['jenis_transaksi'] : '',
		'tahun'								=> $_GET['tahun'],
		'id_jenis_pendapatan'	=> $_GET['id_jenis_pendapatan'],
		'tgl_mulai'						=> $_GET['tgl_mulai'],
		'tgl_akhir'						=> $_GET['tgl_akhir'],
		'bulan_mulai'					=> $_GET['bulan_mulai'],
		'bulan_akhir'					=> $_GET['bulan_akhir'],
	);
	$report = new Report($params);
	$result = $report->arusKas();

	$data['data'] = $result;
	
// 	$app->debug($result);
  

} 
elseif( $aksi == 'entry' ) {
	
	$data['multiple'] = ( isset($_GET['multiple']) ) ? 1 : 0;
	if( $data['multiple'] == 1 ) {
		$aksi = 'entry_multiple';
	}
	$data['request'] = $app->getPembayaran($_GET['id']);
	$data['siswa'] = $app->getSiswa( $data['request']['id_siswa'] );
	$data['kelas'] = $app->getKelas( $data['siswa']['id_kelas'] );
	$data['term'] = $app->getMasterDetails($data['request']['id_jenis_pendapatan']);
	
} 
elseif( $aksi == 'siswa' ) {
	
	$data['data'] = $app->getListSiswa( $_GET['kelas'] );
	if( !empty( $_GET['kelas'] ) ) {
		$data['kelas'] = $app->getKelas($_GET['kelas']);
	}
	
} 
else {
  
  $id_siswa = ( isset($_GET['siswa']) ) ? $_GET['siswa'] : false;
  $data['data'] = $app->getListPembayaran($id_siswa);

}

try {
  
	echo $twig->render( 'default/cetak_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}