<?php

require_once __DIR__ . ("/app.php");

use Port\Excel\ExcelReader;

$table = 'siswa';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$data['dataKelas'] = $app->getKelas();
$data['jenis_pendapatan'] = $app->getMaster('jenis_pendapatan');

$data['months'] = array(
	'01'	=> 'Januari',
	'02'	=> 'Februari',
	'03'	=> 'Maret',
	'04'	=> 'April',
	'05'	=> 'Mei',
	'06'	=> 'Juni',
	'07'	=> 'Juli',
	'08'	=> 'Agustus',
	'09'	=> 'September',
	'10'	=> 'Oktober',
	'11'	=> 'November',
	'12'	=> 'Desember'
);

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) :

	if( isset( $_POST['submit-excel'] ) ) {
		
		if( isset( $_FILES['file_excel']['tmp_name']) && file_exists($_FILES['file_excel']['tmp_name']) ) {
			
			$path = PATH . '/uploads/siswa_import.xls';
			
			if( file_exists( $path ) ) {
				unlink( $path );
			}
			
			$upload = move_uploaded_file( 
				$_FILES['file_excel']['tmp_name'], 
				$path
			);
			
			if( $upload )  {
				
				$file = new \SplFileObject($path);
				$reader = new ExcelReader($file);
				
				$data = array();
				foreach($reader as $row) {
					$data[] = $row;
				}
				unset($data[0]); //header
				$data = array_merge(array(), $data);
				$app->excelImport('siswa', $data);
				$app->add_flash( 'success', 'Data siswa berhasil diimport...' );
				$app->redirect("siswa.php");
			}
			
		}
		
	} 
	else {
		
		$params = array(
			'id'      		=> ( !empty($_GET['id']) ) ? $_GET['id'] : 0,
			'nis'      		=> ( !empty($_POST['nis']) ) ? $_POST['nis'] : '',
			'nisn'      	=> ( !empty($_POST['nisn']) ) ? $_POST['nisn'] : '',
			'id_kelas'    => $_POST['id_kelas'],
			'nama'      	=> $_POST['nama'],
			'email'     	=> $_POST['email'],
			'tpt_lahir'   => $_POST['tpt_lahir'],
			'tgl_lahir'   => $_POST['tgl_lahir'],
			'jk'      		=> $_POST['jk'],
			'alamat'    	=> $_POST['alamat'],
			'kota'      	=> $_POST['kota'],
			'provinsi'    => $_POST['provinsi'],
			'kodepos'   	=> $_POST['kodepos'],
			'agama'     	=> $_POST['agama'],
			'no_hp'     	=> $_POST['no_hp'],
			'tagihan_bulanan'  => ( !empty($_POST['tagihan_bulanan']) ) ? serialize($_POST['tagihan_bulanan']) : null,
			// 'status'   => $_POST['status'],
			// 'created_at' => $_POST['created_at'],
			'updated_at'  => date('Y-m-d H:i:s'),
		);

		$result = $app
			->updateOrInsert( 
				$table, 
				$params,
				$_GET['id']
			);

		$app->add_flash('success', 'Data berhasil disimpan...');
		$app->redirect('siswa.php');
		
	}

else:

	if ( $aksi == 'edit' ) {

		$states = file_get_contents('assets/provinsi.json');
		$states = json_decode($states, true);
		$data['states'] = $states['provinsi']['records'];

		$cities = file_get_contents('assets/kota.json');
		$cities = json_decode($cities, true);
		$data['cities'] = $cities['kota']['records'];

		if ( isset( $_GET['id'] ) ) {

			$data['request'] = $app->getSiswa($_GET['id']);
			$data['request']['tanggungan'] = ( !empty( $data['request']['tagihan_bulanan'] ) ) ? unserialize($data['request']['tagihan_bulanan']) : array();

	// 		$app->debug($data['request']);

		}


	} else {

		$id_kelas = ( !empty( $_GET['kelas'] ) ) ? $_GET['kelas'] : '';
		$data_siswa = $app->getListSiswa($id_kelas);

		if( !empty($_GET['filter']) ) {

			$data['data']['results'] = array();
			foreach( $data_siswa['results'] as $ds ) {

				if( $_GET['filter'] == 'boarding' ) {

					if( in_array( 7, $ds['tagihan_bulanan'] ) ) {
						$data['data']['results'][] = $ds;	
					}

				} elseif( $_GET['filter'] == 'non-boarding' ) {

					if( !in_array( 7, $ds['tagihan_bulanan'] ) ) {
						$data['data']['results'][] = $ds;	
					}

				} else {

					$data['data']['results'] = $data_siswa['results'];
				}
			}

		} else {
			$data['data']['results'] = $data_siswa['results'];
		}

	}
	
endif;

try {

  echo $app->load( 'default/siswa_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {

  die ('ERROR: ' . $e->getMessage());

}