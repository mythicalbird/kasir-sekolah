<?php

require_once __DIR__ . ("/app.php");

$table = 'kas';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$id = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;


if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {

	if( isset( $_POST['submit-pengeluaran'] ) ) {
		
		$id_transaksi = $app->getIdTransaksi('pengeluaran', $id);
		
		$params = array(
			'tanggal'      					=> $_POST['tanggal'],
			'id_jenis_pengeluaran' 	=> $_POST['id_jenis_pengeluaran'],
			'jumlah'   							=> $_POST['jumlah'],
			'id_kas' 								=> $_POST['id_kas'],
			'ket'    								=> $_POST['ket'],
			'id_transaksi'					=> $id_transaksi,
		);

		$result = $app
			->updateOrInsert( 
				'pengeluaran', 
				$params,
				$id
			);
		
		$jenis_pengeluaran = $app->getMasterDetails($params['id_jenis_pengeluaran']);
		$ket_transaksi = ucfirst($jenis_pengeluaran['nama']);
		
		$transaksi_params = array(
			'id_kas'		=> $params['id_kas'],
			'tanggal'		=> $params['tanggal'],
			'jenis'     => 'debet',
			'jumlah' 		=> $params['jumlah'],
			'ket'      	=> $ket_transaksi,
		);

		$result = $app
			->updateOrInsert( 
				'transaksi', 
				$transaksi_params,
				$id_transaksi
			);

		$app->add_flash('success', 'Data berhasil disimpan...');
		$app->redirect( 'kas.php?aksi=pengeluaran' );
		
	} else {
		
		$params = array(
			'jenis'      		=> $_POST['jenis'],
			'nama_akun'     => $_POST['nama_akun'],
			'nama_pemilik'  => $_POST['nama_pemilik'],
			'no_rek' 				=> $_POST['no_rek'],
			'saldo_awal'   	=> $_POST['saldo_awal'],
			'tanggal' 			=> $_POST['tanggal'],
			'keterangan'    => $_POST['keterangan'],
		);

		$result = $app
			->updateOrInsert( 
				$table, 
				$params,
				$id
			);

		$app->add_flash('success', 'Kas berhasil diperbarui...');
		$app->redirect( 'kas.php' );
		
	}

}

if ( $aksi == "edit" ) {

  if ( !empty($_GET['id']) ) {
    $data['request'] = $app->getInfoKas($_GET['id']);
  }

  $data['status_pembayaran'] = $app->getMaster('status_pembayaran');
  $data['jenis_pendapatan'] = $app->getMaster('jenis_pendapatan');
  

} elseif ( $aksi == "pengeluaran" ) { 

	$data['data'] = $app->getListPengeluaran();

} elseif ( $aksi == "pengeluaran_edit" ) { 

  if ( !empty($_GET['id']) ) {
    $data['request'] = $app->getPengeluaran($_GET['id']);
  }
	$data['jenis_pengeluaran'] = $app->getMaster('jenis_pengeluaran');
	$data['akun_kas'] = $app->getListKas();

} else {
  
  $id_siswa = ( isset($_GET['siswa']) ) ? $_GET['siswa'] : false;
  $data['data'] = $app->getListKas($id_siswa);

}

try {
  
	echo $app->load( 'default/kas_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}