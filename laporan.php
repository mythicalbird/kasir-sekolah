<?php

require_once __DIR__ . ("/app.php");

$table = 'pembayaran';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';
$id = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;

$data['dataKelas'] = $app->getListKelas();
$data['status_pembayaran'] = $app->getMaster('status_pembayaran');
$data['jenis_pendapatan'] = $app->getMaster('jenis_pendapatan');
$data['months'] = $months;
$data['years'] = array();
for( $i = date('Y'); $i >= 2000; $i= $i-1 ) {
	$data['years'][] = $i;
}

$data['target_filter'] = ( isset($_POST['jenis_laporan']) ) ? $_POST['jenis_laporan'] : 'harian';

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {

		if( isset( $_POST['submit-laporan'] ) ) {
			
			$args = $_POST;
			$jenis_laporan = $_POST['jenis_laporan'];
			
			$data['status_pembayaran'] = $app->getMaster('status_pembayaran');
			
			$report = new Report();
			
			if( $jenis_laporan == 'harian' ) {
				$laporan = $report->pendapatanHarian( $args );
			} else {
				$laporan = $report->pendapatan( $args );
			}
			
// 			$app->debug($laporan);
			
			$data['data_laporan'] = $laporan;
			
		} 
		elseif( isset( $_POST['submit-laporan-pembayaran'] ) )	{
			

			$bulan_mulai = $_POST['bulan_mulai'];
			$bulan_akhir = $_POST['bulan_akhir'];
			$bulan_arr = array();
			if( !empty($bulan_mulai) || !empty($bulan_akhir) ) {
				if( $bulan_akhir > $bulan_mulai ) {
					for( $i=$bulan_mulai; $i <= $bulan_akhir; $i++  ) {
						$bulan_arr[] = $months[$i];
					}
				} else {
					if( !empty($bulan_mulai) ) {
						$bulan_arr = array($months[$bulan_mulai]);
					} else {
						$bulan_arr = array($months[$bulan_akhir]);
					}
				}
			} else {
				for( $i=1; $i <= count($months); $i++  ) {
					$bulan_arr[] = $months[$i];
				}
			}
			
			$params = array(
				'id_siswa'						=> $_POST['id_siswa'],
				'tahun'								=> $_POST['tahun'],
				'bulan_mulai' 				=> $bulan_mulai,
				'bulan_akhir' 				=> $bulan_akhir,
				'bulan'								=> $bulan_arr,
				'id_jenis_pendapatan'	=> ( !empty($_POST['id_jenis_pendapatan']) ) ? $_POST['id_jenis_pendapatan'] : array()
			);
			
			$report = new Report( $params );
			$result = $report->pembayaran();
			
			$data['laporan'] = $result;
			
// 			$app->debug($result);
			
		} 
		elseif( isset( $_POST['submit-laporan-tagihan'] ) )	{
			
			$params = array(
				'query'								=> $_REQUEST,
				'id_kelas'						=> $_POST['id_kelas'],
				'tahun'								=> $_POST['tahun'],
				'id_jenis_pendapatan'	=> $_POST['id_jenis_pendapatan']
			);
			$report = new Report( $params );
			$result = $report->tagihan();
			
			$data['data'] = $result;
			
// 			$app->debug($result);
			
		} 
		elseif( isset( $_POST['submit-laporan-keuangan'] ) )	{
			
			$params = array(
				'query'								=> $_REQUEST,
				'jenis_laporan'				=> $_POST['jenis_laporan'],
				'jenis_transaksi'			=> ( !empty($_POST['jenis_transaksi']) ) ? $_POST['jenis_transaksi'] : '',
				'tahun'								=> $_POST['tahun'],
				'id_jenis_pendapatan'	=> $_POST['id_jenis_pendapatan'],
				'tgl_mulai'						=> $_POST['tgl_mulai'],
				'tgl_akhir'						=> $_POST['tgl_akhir'],
				'bulan_mulai'						=> $_POST['bulan_mulai'],
				'bulan_akhir'						=> $_POST['bulan_akhir'],
			);
			$report = new Report($params);
			$result = $report->arusKas();
			
			$data['data'] = $result;
			
// 			$app->debug($result);
			
		} 
		else {
			
			$params = array(
				'jenis'      		=> $_POST['jenis'],
				'nama_akun'     => $_POST['nama_akun'],
				'nama_pemilik'  => $_POST['nama_pemilik'],
				'no_rek' 				=> $_POST['no_rek'],
				'saldo_awal'   	=> $_POST['saldo_awal'],
				'tanggal' 			=> $_POST['tanggal'],
				'keterangan'    => $_POST['keterangan'],
			);

			$result = $app
				->updateOrInsert( 
					$table, 
					$params,
					$id
				);

			$app->add_flash('success', 'Profil user berhasil di update.');
			$app->redirect( 'kas.php' );
			
		}

} 
else {
	
	if ( $aksi == "edit" ) {

		if ( !empty($_GET['id']) ) {
			$data['request'] = $app->getUser($_GET['id']);
		}


	} 
	elseif( $aksi == 'kas' ) {

		$params = array();
		$report = new Report( $params );
		$results = $report->arusKas();
		$data['all'] = true;
		
		$data['data'] = array();
		
		foreach( $data['months'] as $k => $v ) {

			$tmp_data = array(
				'bulan'			=> $v,
				'items'			=> array()
			);
			
			foreach( $results['results'] as $res ) {
				$data_m = date('m', strtotime($res['tanggal']));
				if( sprintf("%02d", $data_m) == $k ) {
					$tmp_data['items'][] = $res;
				}
			}
			
			$data['data'][] = $tmp_data;
		}
		

// 		$app->debug( $data['data'] );

	} 
	elseif( $aksi == 'tagihan' ) {

	// 	$app->debug( $data['data'] );

	} 
	else {

		$report = new Report();
		$tmp = $report->pendapatanAll();

		$id_siswa = ( isset($_GET['siswa']) ) ? $_GET['siswa'] : false;
	//   $data['data'] = $app->getListPembayaran($id_siswa);
		$data['data'] = $tmp;

	}
	
}

try {
  
	echo $app->load( 'default/laporan_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}