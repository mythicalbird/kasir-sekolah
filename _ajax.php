<?php

/*
 * Copyright 2018 Media Web Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Port\Excel\ExcelWriter;

require_once dirname(__FILE__) . ("/app.php");



$result = array();
$pdo = new \Slim\PDO\Database(DSN, USR, PWD);





if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {


	if ( !empty( $_POST['aksi'] ) ) {

		$aksi = $_POST['aksi'];

		if ( $aksi == "data" ) {
			
			$table = $_POST['data'];
			
			if ( $table == "master" && isset( $_POST['type'] ) ) {
				
				$type = $_POST['type'];
				
				if( $type == 'jumlah_pembayaran' ) {
					
					$param = $_POST['param'];
					$siswa = $app->getSiswa( $param['id_siswa'] );
					$selectStatement = $pdo->select()
											 ->from($table)
											 ->whereMany(array(
											 		'type'	=> $type,
												 	'nama'	=> $param['id_jenis_pendapatan'],
												 	'ket'		=> $siswa['id_kelas']
											 ), '=');
					$stmt = $selectStatement->execute();
					$data = $stmt->fetch();

					$result = (int)$data['custom_1'];
				}	

			} 
			else {

				$selectStatement = $pdo->select()
									   ->from($table);
				
				if( isset($_POST['id']) ) {
					$selectStatement->where('id', '=', $_POST['id']);
					$stmt = $selectStatement->execute();
					$data = $stmt->fetch();
					
					if( $table == 'master' && $data['type'] == "jumlah_pembayaran" ) {
						$data['custom_1'] = (int)$data['custom_1'];
					}

					$result = $data;
					
				} else {
					
					$stmt = $selectStatement->execute();
					$data = $stmt->fetchAll();

					$result['items'] = $data;
					
				}

			}


		} 
		elseif( $aksi == "export" ) {
			
			$file = new \SplFileObject('uploads/siswa_export.xlsx', 'w');
			$writer = new ExcelWriter($file);
			
			$tbHeader = ['NIS', 'NAMA', 'KELAS', 'TEMPAT LAHIR', 'TANGGAL LAHIR', 'JENIS KELAMIN', 'AGAMA', 'EMAIL', 'NOMOR HP', 'ALAMAT', 'KOTA', 'PROPINSI', 'KODEPOS'];

			$writer->prepare();
			$writer->writeItem($tbHeader);
			
			$selectStatement = $pdo->select()
				->from('siswa');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
			
			foreach( $data as $row ) {
				$kelas = $app->getKelas($row['id_kelas']);
				$writer->writeItem(array(
					'NIS'							=> $row['nis'], 
					'NAMA' 						=> $row['nama'], 
					'KELAS'						=> ( false !== $kelas ) ? $kelas['nama_kelas'] : '', 
					'TEMPAT LAHIR'		=> $row['tpt_lahir'], 
					'TANGGAL LAHIR'		=> $row['tgl_lahir'], 
					'JENIS KELAMIN'		=> $row['jk'], 
					'AGAMA'						=> $row['agama'], 
					'EMAIL'						=> $row['email'], 
					'NOMOR HP'				=> $row['no_hp'], 
					'ALAMAT'					=> $row['alamat'], 
					'KOTA'						=> $row['kota'], 
					'PROPINSI'				=> $row['provinsi'], 
					'KODEPOS'					=> $row['kodepos']
				));
			}
			
			$writer->finish();
			
		} 
		elseif( $aksi == "tanggungan_siswa" ) {
			
			$siswa = $app->getSiswa( $_POST['id_siswa'] );
			if( !$siswa ) {
				return;
			}
			$html = '';
			$tanggungan = ( !empty( $siswa['tagihan_bulanan'] ) ) ? unserialize($siswa['tagihan_bulanan']) : array();
			foreach( $tanggungan as $id_jenis_pendapatan ) {
				
				$html .= '<tr>';
				$html .= '<td>' . $app->getMasterNamaTerm($id_jenis_pendapatan) . '</td>';
				
				foreach( $months as $month ) {
					$html .= '<td>';
					$tagihan = $app->getTanggunganSiswaByMonth( $siswa['id'], $id_jenis_pendapatan, $month );
					if( ! $tagihan ) {
						$html .= '-';
					} else {
						$jumlah = number_format($tagihan['jumlah'],0);
						if( $tagihan['status'] == 11 ) {
							$html .= '<span class="text-green">'.$jumlah.'</span>';
						}
					}
					$html .= '</td>';
				}
				
				$html .= '</tr>';

// 					$selectStatement = $this->pdo->select()
// 						->from('pembayaran')
// 						->where('id_siswa', '=', $id_siswa, 'AND')
// 						->where('id_jenis_pendapatan', '=', $id_jenis_pendapatan);
// 					$stmt = $selectStatement->execute();
// 					$data = $stmt->fetch();

// 					if( false !== $data ) {
// 						$ret['total_items']++;
// 						$ret['results'][] = $data;
// 					}
			}
			$result = $html;
			
			
		} 
		elseif( $aksi == "dumpsql" ) {
			
			$app->dumpSql();

		} 
		elseif ( $aksi == "hapus" ) {
			
			$table = $_POST['data'];
			$id = $_POST['id'];
			
			if( $table == "pembayaran" ) {
				
				$x = $app->getPembayaran($id);
				$deleteStatement = $pdo->delete()
					->from('transaksi')
					->where('id', '=', $x['id_transaksi']);
				$affectedRows = $deleteStatement->execute();
				
			} elseif( $table == "pengeluaran" ) {
				
				$x = $app->getPengeluaran($id);
				$deleteStatement = $pdo->delete()
					->from('transaksi')
					->where('id', '=', $x['id_transaksi']);
				$affectedRows = $deleteStatement->execute();
				
			}

			$deleteStatement = $pdo->delete()
				->from($table)
				->where('id', '=', $id);
			$affectedRows = $deleteStatement->execute();
			$app->add_flash('success', 'Data berhasil dihapus');

		}

	}

}

/**
 * Autocomplete Select2 Ajax Remote Data
 */
if ( !empty($_GET['term']) ) {

	$table = 'siswa';
	$selectStatement = $pdo->select()
							 					 ->from($table)
												 ->whereLike('nama', '%'.$_GET['term'].'%');
	$stmt = $selectStatement->execute();
	$data = $stmt->fetchAll();
	
	$response_data = array();
	foreach($data as $res) {
		$kelas = $app->getKelas($res['id_kelas']);
		$response_data[] = array(
			'id'				=> $res['id'],
			'name'			=> $res['nama'],
			'full_name'	=> $res['nama'],
      "kelas"			=> ( false !== $kelas ) ? 'Kelas ' . $kelas['nama_kelas'] : '',
		);
	}
	
	$result = array(
		'total_count'					=> count($data),
		'incomplete_results'	=> true,
		'items'								=> $response_data,
	);
	

}

/**
 * X-editable
 * @param pk | name | value
 */
if ( $_SERVER['REQUEST_METHOD'] == "POST" && !empty($_POST['pk']) ) {

	$table = ( !empty( $_GET['data'] ) ) ? $_GET['data'] : 'master';
	$field = ( !empty( $_GET['field'] ) ) ? $_GET['field'] : 'nama';
	$updateStatement = $pdo->update(array($field => $_POST['value']))
												 ->table($table)
												 ->where('id', '=', $_POST['pk']);
	$result = $updateStatement->execute();

}


/**
 * DataTable Source Data
 */
if( !empty( $_GET['datatable'] ) ) {
	$result['data'] = $app->getListPembayaran(11);
}

echo json_encode($result);