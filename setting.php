<?php

require_once __DIR__ . ("/app.php");

$table = 'users';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';

$data['target_tab'] = ( !empty($_GET['target']) ) ? $_GET['target'] : 'details';

if ( isset( $_POST['submit-setting'] ) ) {

  $args = array(
//     'logo_image_width'          => $_POST['logo_image_width'],
//     'logo_image_height'         => $_POST['logo_image_height'],
    'app_name'          => $_POST['app_name'],
    'app_admin_email'		=> $_POST['app_admin_email'],
		'app_description'		=> $_POST['app_description'],
    'nama_sekolah'      => $_POST['nama_sekolah'],
		'tahun_ajaran'      => $_POST['tahun_ajaran']
  );

  if( isset($_FILES['logo_image']['tmp_name']) && file_exists($_FILES['logo_image']['tmp_name']) ) {
    if( file_exists(PATH . '/uploads/' . get_option('logo_image')) ) unlink(PATH . '/uploads/' . get_option('logo_image'));
    $args['logo_image'] = basename(date('YmdHis') . '-' . $_FILES['logo_image']['name']);
    move_uploaded_file($_FILES['logo_image']['tmp_name'], PATH . '/uploads/' . $args['logo_image']);
  }

  foreach ($args as $key => $value) {
    Mwt::update_option(trim($key), trim($value));
  }
  
  $app->add_flash( 'success', 'Pengaturan berhasil disimpan.' );
	$app->redirect( 'setting.php' );

}
elseif( isset( $_POST['submit-credentials'] ) )  {
	
  if( isset($_FILES['credential_file']['tmp_name']) && file_exists($_FILES['credential_file']['tmp_name']) ) {
    if( file_exists( PATH . '/app/client_credentials.json' ) ) {
			unlink(PATH . '/app/client_credentials.json');
		}
    move_uploaded_file( 
			$_FILES['credential_file']['tmp_name'], 
			PATH . '/app/client_credentials.json'
		);
		$app->add_flash( 'success', 'Credential berhasil diupload.' );
		$app->redirect( 'setting.php?target=backup' );
  }
	
}

require_once __DIR__ . ("/inc/Google/google-drive.php");

try {

  echo $app->load( 'default/setting_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {

  die ('ERROR: ' . $e->getMessage());

}