<?php

require_once __DIR__ . ("/app.php");

$table = 'users';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {


  if ( isset($_GET['id']) ) {
    $id = $_GET['id'];
    $key = array('id' => $id);
  }
  $data = array(
    // 'kode'      => $_POST['kode'],
    'nama_kelas'  => $_POST['nama_kelas'],
    'keterangan'  => $_POST['keterangan'],
  );

  if ( isset($_GET['id']) ) {
    $affectedRow = updateMany('kelas', $key, $data, array('id', '=', $id));
  } else {
    $column = array_keys($data);
    $values = array_values($data);
    $id = insert('kelas', $column, $values, true);
  }

  $app->add_flash('success', 'Data berhasil disimpan');
	$app->redirect('kelas.php');

}

if ( $aksi == 'edit' ) {

  if ( isset( $_GET['id'] ) ) {
    
    $data['request'] = $app->getKelas($_GET['id']);

  }


} else {

  $data['data'] = $app->getKelas();

}

try {

  echo $app->load( 'default/kelas_'.$aksi.'.html.twig', $data );
  
} catch (Exception $e) {

  die ('ERROR: ' . $e->getMessage());

}