<?php

require_once __DIR__ . ("/app.php");

if ( isset($_GET['action']) && $_GET['action'] == "logout" ) 
	Mwt::signOut( 'login.php?logout=true' );

if( $_SERVER['REQUEST_METHOD'] == "POST" ) {

	$username = $_POST['username'];
	$password = $_POST['password'];
	$remember = ( isset($_POST['rememberme']) ) ? 1 : 0 ;

	$login = $app->signIn($username, $password, $remember);

	if( $login['success'] != 1 ) {
		$app->redirect('login.php?error=1');
	}

	$app->redirect('index.php');

}

try {

	echo $twig->render('default/login.html.twig', $data);
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}