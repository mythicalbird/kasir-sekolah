<?php 
return array (
  'debug' => true,
  'timezone' => 'Asia/Jakarta',
  'locale' => 'id_ID',
  'locale_date_format' => '%A, %d %B %G',
  'locale_time_format' => ' %T',
  'db' => 
  array (
    'type' => 'mysql',
    'host' => 'localhost',
    'name' => 'db_kasir_sekolah',
    'user' => 'root',
    'password' => 'root',
  ),
  'twig' => 
  array (
    'debug' => true,
    'auto_reload' => true,
//     'cache' => __DIR__ . '/cache/',
		'cache' => false,
  )
);