<?php

$term = ( !empty($_GET['term']) ) ? $_GET['term'] : '';

$data['master'] = $app->getMaster($term);

$table = 'master';

if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {

	$id = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;

	$params = array(
		'id'				=> ( !empty($_POST['id']) ) ? $_POST['id'] : $id,
		'type'			=> $term,
		'kode'			=> ( !empty($_POST['kode']) ) ? $_POST['kode'] : '',
		'nama'			=> $_POST['nama'],
		'ket'				=> ( !empty($_POST['ket']) ) ? $_POST['ket'] : '',
		'custom_1'	=> ( !empty($_POST['custom_1']) ) ? $_POST['custom_1'] : '',
		'custom_2'	=> ( !empty($_POST['custom_2']) ) ? $_POST['custom_2'] : '',
		'custom_3'	=> ( !empty($_POST['custom_3']) ) ? $_POST['custom_3'] : '',
	);

	$result = $app
		->updateOrInsert( 
			$table,
			$params,
			$params['id'] 
		);

	
	$app->add_flash( 'success', 'Data berhasil disimpan...' );
	$app->redirect( 'index.php?_uri=master&term='.$term );
	
}

$template = ( file_exists( PATH . '/views/default/master_'.$term.'.html.twig' ) ) ? $term : 'index';
$template = 'default/master_'.$template.'.html.twig';

if( $term == 'jenis_pendapatan' ) {
	$users = $app->getListUser(array( 'level' => 3 ));
	$data['users'] = $users['results'];
} elseif( $term == 'jumlah_pembayaran' ) {
	$data['jenis_pendapatan'] = $app->getMaster('jenis_pendapatan');
	$data['listKelas'] = $app->getListKelas();
}

echo $twig->render($template, $data);