<?php

/*
 * Copyright 2018 Media Web Technology.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

include dirname(__FILE__) . '/../vendor/autoload.php';
require dirname(__FILE__) . '/../inc/Mwt.php';
require dirname(__FILE__) . '/../inc/Report.php';
require dirname(__FILE__) . '/../inc/Twig/AppExtension.php';

// HELPERS
require dirname(__FILE__) . '/../inc/Helpers/helpers.php';

$app = new Mwt();

if( basename($_SERVER['REQUEST_URI']) != "login.php" ):

	if ( !isset( $_SESSION['_islogged'] ) || $_SESSION['_islogged'] !== true ) {
		$app->redirect("login.php");
	}

endif;

$data = array();
$months = array(
	1		=> 'Januari',
	2		=> 'Februari',
	3		=> 'Maret',
	4		=> 'April',
	5		=> 'Mei',
	6		=> 'Juni',
	7		=> 'Juli',
	8		=> 'Agustus',
	9		=> 'September',
	10	=> 'Oktober',
	11	=> 'November',
	12	=> 'Desember'
);