<?php

class Report extends Mwt
{

	public $months = array(
		'01'	=> 'Januari',
		'02'	=> 'Februari',
		'03'	=> 'Maret',
		'04'	=> 'April',
		'05'	=> 'Mei',
		'06'	=> 'Juni',
		'07'	=> 'Juli',
		'08'	=> 'Agustus',
		'09'	=> 'September',
		'10'	=> 'Oktober',
		'11'	=> 'November',
		'12'	=> 'Desember'
	);
	
	public $params;
	
	public function __construct( $params = array() ) {
		parent::__construct();
		$this->params = $params;
	}

	public function pendapatan( $args = array() ) {
		
		$jenis_laporan = $args['jenis_laporan'];
		$jenis_pendapatan= $args['jenis_pendapatan'];
		
		$response = array(
			'query'		=> $args,
			'results'	=> array()
		);
		$status_pembayaran = $this->getMaster('status_pembayaran');
		$total = array();
		foreach( $status_pembayaran as $status ) {
			$total['status_'.$status['id']] = 0;
		}
		
		if( $jenis_laporan == 'bulanan' ) {

				$bulan_mulai = (int) $args['bulan_mulai'];
				$bulan_akhir = (int) $args['bulan_akhir'];
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->where('YEAR(tanggal)', '=', $args['tahun_bulanan'], 'AND')
																		 ->whereBetween('MONTH(tanggal)', array( $bulan_mulai, $bulan_akhir ));
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();

				foreach( $data as $dl ){

					$key = date('m', strtotime($dl['tanggal']));

					if( !isset( $response['results'][$key] ) ) {

						$response['results'][$key] = array();
						$response['results'][$key]['bulan'] = date('m', strtotime($dl['tanggal']));

						foreach( $status_pembayaran as $status ) {
							$response['results'][$key]['status_'.$status['id']] = 0;
						}

					} 

					foreach( $status_pembayaran as $status ) {
						if( $dl['status'] === $status['id'] ) {
							$response['results'][$key]['status_'.$status['id']] += $dl['jumlah'];
						}
					}

				}

		} elseif( $jenis_laporan == 'mingguan' ) {

				$bulan = (int) $args['bulan_mingguan'];
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->where('YEAR(tanggal)', '=', $args['tahun_mingguan'], 'AND')
																		 ->where('MONTH(tanggal)', '=', $bulan);
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();
			
				$response['results'] = array(
					'1'	=> array(),
					'2'	=> array(),
					'3'	=> array(),
					'4'	=> array(),
				);

				foreach( $data as $dl ){

					$tanggal = date('d', strtotime($dl['tanggal']));
					
					if( $tanggal > 21 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['4']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} elseif( $tanggal > 14 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['3']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} elseif( $tanggal > 7 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['2']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} else {

						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['1']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					}

				}

				

		} else {

				$tgl_mulai = date('Y-m-d', strtotime($args['tgl_mulai'] . ' -1 day'));
				$tgl_akhir = date('Y-m-d', strtotime($args['tgl_akhir'] . ' +1 day'));
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->whereBetween('tanggal', array( $tgl_mulai, $tgl_akhir ));
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();

				foreach( $data as $dl ){

					$key = date('Ymd', strtotime($dl['tanggal']));

					if( !isset( $response['results'][$key] ) ) {

						$response['results'][$key] = array();
						$response['results'][$key]['tanggal'] = $dl['tanggal'];

						foreach( $status_pembayaran as $status ) {
							$response['results'][$key]['status_'.$status['id']] = 0;
						}

					} 

					foreach( $status_pembayaran as $status ) {
						if( $dl['status'] === $status['id'] ) {
							$response['results'][$key]['status_'.$status['id']] += $dl['jumlah'];
						}
					}

				}

		}
		
		// get total 
		foreach( $response['results'] as $res ) {
			foreach( $status_pembayaran as $status ) {
				$total['status_'.$status['id']] += $res['status_'.$status['id']];
			}
		}		
		$response['total'] = $total;
		return $response;
		
	}
	
	public function pendapatanBerdasarkanStatus( $args = array() ) 
	{
		
		$jenis_laporan = $args['jenis_laporan'];
		$jenis_pendapatan= $args['jenis_pendapatan'];
		
		$response = array(
			'query'		=> $args,
			'results'	=> array()
		);
		$status_pembayaran = $this->getMaster('status_pembayaran');
		$total = array();
		foreach( $status_pembayaran as $status ) {
			$total['status_'.$status['id']] = 0;
		}
		
		if( $jenis_laporan == 'bulanan' ) {

				$bulan_mulai = (int) $args['bulan_mulai'];
				$bulan_akhir = (int) $args['bulan_akhir'];
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->where('YEAR(tanggal)', '=', $args['tahun_bulanan'], 'AND')
																		 ->whereBetween('MONTH(tanggal)', array( $bulan_mulai, $bulan_akhir ));
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();

				foreach( $data as $dl ){

					$key = date('m', strtotime($dl['tanggal']));

					if( !isset( $response['results'][$key] ) ) {

						$response['results'][$key] = array();
						$response['results'][$key]['bulan'] = date('m', strtotime($dl['tanggal']));

						foreach( $status_pembayaran as $status ) {
							$response['results'][$key]['status_'.$status['id']] = 0;
						}

					} 

					foreach( $status_pembayaran as $status ) {
						if( $dl['status'] === $status['id'] ) {
							$response['results'][$key]['status_'.$status['id']] += $dl['jumlah'];
						}
					}

				}

		} elseif( $jenis_laporan == 'mingguan' ) {

				$bulan = (int) $args['bulan_mingguan'];
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->where('YEAR(tanggal)', '=', $args['tahun_mingguan'], 'AND')
																		 ->where('MONTH(tanggal)', '=', $bulan);
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();
			
				$response['results'] = array(
					'1'	=> array(),
					'2'	=> array(),
					'3'	=> array(),
					'4'	=> array(),
				);

				foreach( $data as $dl ){

					$tanggal = date('d', strtotime($dl['tanggal']));
					
					if( $tanggal > 21 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['4']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} elseif( $tanggal > 14 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['3']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} elseif( $tanggal > 7 ) {
						
						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['2']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					} else {

						foreach( $status_pembayaran as $status ) {
							if( $dl['status'] === $status['id'] ) {
								$response['results']['1']['status_'.$status['id']] += $dl['jumlah'];
							}
						}
						
					}

				}

				

		} else {

				$tgl_mulai = date('Y-m-d', strtotime($args['tgl_mulai'] . ' -1 day'));
				$tgl_akhir = date('Y-m-d', strtotime($args['tgl_akhir'] . ' +1 day'));
			
				$selectStatement = $this->pdo->select()
																		 ->from('pembayaran')
																		 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																		 ->whereBetween('tanggal', array( $tgl_mulai, $tgl_akhir ));
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();

				foreach( $data as $dl ){

					$key = date('Ymd', strtotime($dl['tanggal']));

					if( !isset( $response['results'][$key] ) ) {

						$response['results'][$key] = array();
						$response['results'][$key]['tanggal'] = $dl['tanggal'];

						foreach( $status_pembayaran as $status ) {
							$response['results'][$key]['status_'.$status['id']] = 0;
						}

					} 

					foreach( $status_pembayaran as $status ) {
						if( $dl['status'] === $status['id'] ) {
							$response['results'][$key]['status_'.$status['id']] += $dl['jumlah'];
						}
					}

				}

		}
		
		// get total 
		foreach( $response['results'] as $res ) {
			foreach( $status_pembayaran as $status ) {
				$total['status_'.$status['id']] += $res['status_'.$status['id']];
			}
		}		
		$response['total'] = $total;
		return $response;
		
	}
	
	public function pendapatanHarian( $args = array() ) 
	{
		
		$jenis_laporan = $args['jenis_laporan'];
		$jenis_pendapatan= $args['jenis_pendapatan'];
		
		$response = array(
			'query'		=> $args,
			'results'	=> array()
		);
		$status_pembayaran = $this->getMaster('status_pembayaran');
		$total = array();
		foreach( $status_pembayaran as $status ) {
			$total['status_'.$status['id']] = 0;
		}
		
		$tgl_mulai = date('Y-m-d', strtotime($args['tgl_mulai'] . ' -1 day'));
		$tgl_akhir = date('Y-m-d', strtotime($args['tgl_akhir'] . ' +1 day'));

		$selectStatement = $this->pdo->select()
																 ->from('pembayaran')
																 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
																 ->whereBetween('tanggal', array( $tgl_mulai, $tgl_akhir ));
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();

		foreach( $data as $dl ){

			$key = date('Ymd', strtotime($dl['tanggal']));

			if( !isset( $response['results'][$key] ) ) {

				$response['results'][$key] = array();
				$response['results'][$key]['tanggal'] = $dl['tanggal'];

				foreach( $status_pembayaran as $status ) {
					$response['results'][$key]['status_'.$status['id']] = 0;
				}

			} 

			foreach( $status_pembayaran as $status ) {
				if( $dl['status'] === $status['id'] ) {
					$response['results'][$key]['status_'.$status['id']] += $dl['jumlah'];
				}
			}
			
			$response['results'][$key]['total'] = 0;
			foreach( $status_pembayaran as $status ) {
				$response['results'][$key]['total'] += $response['results'][$key]['status_'.$status['id']];
			}
			
		}
		
		// get total 
		foreach( $response['results'] as $res ) {
			foreach( $status_pembayaran as $status ) {
				$total['status_'.$status['id']] += $res['status_'.$status['id']];
			}
		}		
		$response['total'] = $total;
		return $response;
		
	}
	
	public function pendapatanAll($args=array()) 
	{
		
		// $jenis_laporan = $args['jenis_laporan'];
		// $jenis_pendapatan= $args['jenis_pendapatan'];
		
		$response = array(
			'query'		=> $args,
			'results'	=> array()
		);
		$status_pembayaran = $this->getMaster('status_pembayaran');
		$jenis_pendapatan = $this->getMaster('jenis_pendapatan');
		$total = array();
		foreach( $status_pembayaran as $status ) {
			$total['status_'.$status['id']] = 0;
		}
		
		$tgl_mulai = date('Y-m-d', strtotime($args['tgl_mulai'] . ' -1 day'));
		$tgl_akhir = date('Y-m-d', strtotime($args['tgl_akhir'] . ' +1 day'));

		$selectStatement = $this->pdo->select()
																 ->from('pembayaran');
// 																 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
// 																 ->whereBetween('tanggal', array( $tgl_mulai, $tgl_akhir ));
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();

		foreach( $data as $dl ){

			$key = date('Ymd', strtotime($dl['tanggal']));

			if( !isset( $response['results'][$key] ) ) {

				$response['results'][$key] = array();
				$response['results'][$key]['tanggal'] = $dl['tanggal'];
				
				foreach( $jenis_pendapatan as $jp ) {
					$response['results'][$key]['pembayaran_'.$jp['id']] = array();
					
					
					foreach( $status_pembayaran as $status ) {
						$response['results'][$key]['pembayaran_'.$jp['id']]['status_'.$status['id']] = 0;
					}
					
				}

			} 

			foreach( $jenis_pendapatan as $jp ) {
				
				foreach( $status_pembayaran as $status ) {
					
					if( $dl['id_jenis_pendapatan'] == $jp['id'] && $dl['status'] === $status['id'] ) {
						$response['results'][$key]['pembayaran_'.$jp['id']]['status_'.$status['id']] += $dl['jumlah'];
					}
				}
				
				$response['results'][$key]['pembayaran_'.$jp['id']]['total'] = 0;
				foreach( $status_pembayaran as $status ) {
					$response['results'][$key]['pembayaran_'.$jp['id']]['total'] += $response['results'][$key]['pembayaran_'.$jp['id']]['status_'.$status['id']];
				}

			}
			
		}
		
		// get total 
		foreach( $response['results'] as $res ) {
			foreach( $status_pembayaran as $status ) {
				$total['status_'.$status['id']] += $res['status_'.$status['id']];
			}
		}		
		$response['total'] = $total;
		

		$chartData = array(
			'nama_chart'	=> 'Laporan Pembayaran',
			'columns'			=> array(
				'head'	=> array(),
				'data'	=> array()
			)
		);
		foreach( $status_pembayaran as $status ) {
			$chartData['columns']['head'][] = $status['nama'];
		}
		$response['chart_data'] = $this->generateBarChartData($chartData);
		
		return $response;
		
	}
	
	public function pembayaranBySiswa($id_siswa, $params=array()) {
		$ret = array();
		$default_params = array(
			'id_jenis_pendapatan'	=> false,
			'tahun'								=> null,
		);
		foreach( $default_params as $k => $v ) {
			if( !isset( $params[$k] ) )
				$params[$k] = $v;
		}
		if( is_null( $params['tahun'] ) ) {
			$params['tahun'] = date("Y");
		}
		$ret['tahun'] = $params['tahun'];
		$ret['total_items'] = 0;
		$ret['results'] = array();
		
		$id_jenis_pendapatan = $params['id_jenis_pendapatan'];
		$siswa = $this->getSiswa($id_siswa);
		
		if( ! $id_jenis_pendapatan ) {
			
			$tanggungan = ( !empty( $siswa['tagihan_bulanan'] ) ) ? unserialize($siswa['tagihan_bulanan']) : array();


			foreach( $tanggungan as $id_jenis_pendapatan ) {

					$selectStatement = $this->pdo->select()
						->from('pembayaran')
						->where('id_siswa', '=', $id_siswa, 'AND')
						->where('id_jenis_pendapatan', '=', $id_jenis_pendapatan);
					$stmt = $selectStatement->execute();
					$data = $stmt->fetch();

					if( false !== $data ) {
						$ret['total_items']++;
						$ret['results'][] = $data;
					}
			}
			
		} else {
			
			$selectStatement = $this->pdo->select()
				->from('pembayaran')
				->where('id_siswa', '=', $id_siswa, 'AND')
				->where('id_jenis_pendapatan', '=', $id_jenis_pendapatan);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
			
			$ret['total_items'] = count($data);
			$ret['results'] = $data;
		}
		
		return $ret;
	}
	
	public function arusKas()
	{
		$default_params = array(
			'query'								=> $_REQUEST,
			'jenis_laporan'				=> 'all',
			'id_kelas'						=> 0,
			'pagination'					=> false,
			'page'								=> ( !empty( $_GET['page'] ) ) ? $_GET['page'] : 1,
			'limit'								=> 25,
			'order_by'						=> 'id',
			'order'								=> 'DESC',
			'id_jenis_pendapatan'	=> '',
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($this->params[$k]) )
				$this->params[$k] = $v;
		}
		$this->response['results'] = array();
		$this->response['query'] = $this->params['query'];
	
		if( $this->params['jenis_laporan'] == 'all' ) {
			
			$selectStatement = $this->pdo->select()
				->from('transaksi');
			$stmt = $selectStatement->execute();
			$results = $stmt->fetchAll();
			
		} elseif( $this->params['jenis_laporan'] == 'harian' ) {
				
			$selectStatement = $this->pdo->select()
				->from('transaksi');
			if( !empty( $this->params['jenis_transaksi'] ) ) {
				$selectStatement->where('jenis', '=', $this->params['jenis_transaksi'], 'AND');
			}
			$selectStatement->whereBetween('tanggal', array( $this->params['tgl_mulai'], $this->params['tgl_akhir'] ));
			$stmt = $selectStatement->execute();
			$results = $stmt->fetchAll();
				
		} elseif( $this->params['jenis_laporan'] == 'bulanan' ) {
			
			$selectStatement = $this->pdo->select()
				->from('transaksi');
			if( !empty( $this->params['jenis_transaksi'] ) ) {
				$selectStatement->where('jenis', '=', $this->params['jenis_transaksi'], 'AND');
			}
			$selectStatement
				->where('YEAR(tanggal)', '=', $this->params['tahun'], 'AND')
				->whereBetween('MONTH(tanggal)', array( $this->params['bulan_mulai'], $this->params['bulan_akhir'] ));
			$stmt = $selectStatement->execute();
			$results = $stmt->fetchAll();
			
			
		}
		
		for( $i = 0; $i < count($results); $i++ ) {
			if( !empty( $this->params['id_jenis_pendapatan'] ) ) {
				$selectStatement = $this->pdo->select()
					->from('pembayaran')
					->whereMany(array(
						'id_transaksi'	=> $results[$i]['id'],
						'id_jenis_pendapatan'	=> $this->params['id_jenis_pendapatan']
					), '=');
				$stmt = $selectStatement->execute();
				$pembayaran = $stmt->fetch();
				if( false !== $pembayaran ) {
					$results[$i]['kas'] = $this->getInfoKas($results[$i]['id_kas']);
				} else {
					unset($results[$i]);
				}
			} else {
				$results[$i]['kas'] = $this->getInfoKas($results[$i]['id_kas']);
			}
		}
		
		$this->response['results'] = array_merge(array(), $results);
		
		return $this->response;
	}
	
	public function generateBarChartData($params = array()) {
		
		$default_params = array(
			'nama_chart'	=> '',
			'columns'			=> array()
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($params[$k]) ) 
				$params[$k] = $v;
		}
		
		$columns = $params['columns'];
		
		global $app;
		$chartRows = array();


		$data = array();
		// ambil data asal daerah base on tptLahir
// 		$dataUser = $em->getRepository('AppBundle:User')
// 				->findByHakAkses($this->appService->getMasterTermObject('hak_akses', 4));
// 		foreach ($dataUser as $u) {
// 				if ( $u->getTptLahir() != '' ) {
// 						array_push($data, $u->getTptLahir());
// 				}
// 		}
// 		$data = array_unique($data);
		
		$jenis_pendapatan = $app->getMaster('jenis_pendapatan');
		
		$columns_head = [ $params['nama_chart'] ];
		$columns_head = array_merge( $columns_head, $columns['head'] );
		
		$chartRows[] = $columns_head;
		
		foreach( $jenis_pendapatan as $jp ) {
			$chartRows[] = [ $jp['nama'], rand(), mt_rand() ];
		}

// 		for ( $i = 0; $i < count($data); $i++ ) {
// 				if (isset($data[$i])) {
// 						$count = 0;
// 						$mhsData = $em->getRepository('AppBundle:Mahasiswa')
// 								->findBy(array(
// 									'maba'        => 0,
// 									'status'      => $this->appService->getMasterTermObject('status_mahasiswa', 'A')
// 								));
// 						if ( $mhsData ) {
// 								foreach ($mhsData as $mhs) {
// 										if ( null !== $mhs->getUser() && $mhs->getUser()->getTptLahir() == $data[$i] ) {
// 												$count++;
// 										}
// 								}
// 						}
// 						$chartRows[] = array( $data[$i], $count );
// 				}
// 		}

// 		$pieChart = array(
// 				'options'     => array(
// 								'title' => 'Presentasi Data Jenis kelamin',
// 								// 'width' => 400,
// 								// 'height'=> 300
// 				),
// 				'rows'     => $chartRows
// 		);

		$barChart = array(
				'options'     => array(
								'title' => 'Grafik Data Jenis kelamin',
								// 'width' => 400,
								// 'height'=> 300
				),
				'rows'     => json_encode($chartRows)
		);
		
		return $barChart;
	}
	
	public function pembayaranBulananSiswa($id_siswa, $id_jenis_pendapatan=array(), $months=array(), $tahun=null) {
		
		global $app;
		
		$ret = array(
			'query'				=> array(
				'id_siswa'	=> (int)$id_siswa,
			),
			'total_items' => 0,
			'results'			=> array()
		);
		
		if( count($id_jenis_pendapatan) == 0 ) {
			$jenis_pendapatan = $app->getMaster('jenis_pendapatan');
		} else {
			$selectStatement = $this->pdo->select()
				->from('master')
				->whereIn( 'id', $id_jenis_pendapatan );
			$stmt = $selectStatement->execute();
			$jenis_pendapatan = $stmt->fetchAll();
		}
		
		$ret['query']['jenis_pendapatan'] = $jenis_pendapatan;
		
		if( count($months) == 0 ) {
			$months = $this->months;
		}
		
		if( is_null($tahun) ) {
			$tahun = date('Y');
		}
		$ret['query']['tahun'] = $tahun;
		
		$siswa = $app->getSiswa($id_siswa);
		$ret['query']['siswa'] = $siswa;
		
		$months = array_values($months);
		
		for ( $i = 0; $i < count($months); $i++ ) {
			
			$ret['results'][$i]['bulan'] = $months[$i];
			$ret['results'][$i]['pembayaran'] = array();
			
			foreach($jenis_pendapatan as $jp) {
				$tagihan = $app->getTanggunganSiswaByMonth( $siswa['id'], $jp['id'], $months[$i] );
				
				$ret['results'][$i]['pembayaran'][] = array(
					'nama_tagihan'	=> $jp['nama'],
					'jumlah'				=> ( ! $tagihan ) ? 0 : $tagihan['jumlah'],
					'status'				=> ( ! $tagihan ) ? '' : $app->getMasterNamaTerm($tagihan['status']),
					'id_status'			=> ( ! $tagihan ) ? '' : $tagihan['status'],
				);

			}
		}
		
// 		$selectStatement = $this->pdo->select()
// 																 ->from('pembayaran')
// 																 ->where('id_jenis_pendapatan', '=', $jenis_pendapatan, 'AND')
// 																 ->whereBetween('tanggal', array( $tgl_mulai, $tgl_akhir ));
// 		$stmt = $selectStatement->execute();
// 		$data = $stmt->fetchAll();
		
		return $ret;
	}
	
	public function pembayaran() {
		$default_params = array(
			'query'								=> $_REQUEST,
			'id_kelas'						=> 0,
			'pagination'					=> false,
			'page'								=> ( !empty( $_GET['page'] ) ) ? $_GET['page'] : 1,
			'limit'								=> 25,
			'order_by'						=> 'id',
			'order'								=> 'DESC',
			'id_jenis_pendapatan'	=> array(),
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($this->params[$k]) )
				$this->params[$k] = $v;
		}
		$this->response['results'] = array();
		$this->response['query'] = $this->params['query'];
		$this->response['query']['bulan'] = $this->params['bulan'];
		$siswa = $this->getSiswa($this->params['id_siswa']);
		$this->response['siswa'] = $siswa;
		$tagihan_siswa = unserialize( $siswa['tagihan_bulanan'] );
		
		$selectStatement = $this->pdo->select()
			->from('pembayaran');
		if( count($this->params['id_jenis_pendapatan']) > 0 ) {
			$selectStatement
			->where('id_siswa', '=', $siswa['id'], 'AND')
			->whereIn('id_jenis_pendapatan', $this->params['id_jenis_pendapatan']);			
		} else {
			$selectStatement
			->where('id_siswa', '=', $siswa['id']);
		}
		$stmt = $selectStatement->execute();
		$pembayaran_results = $stmt->fetchAll();
	
		foreach( $pembayaran_results as $pr ) {
// 			if( null !== $sr['tagihan_bulanan'] ) {
// 				$tagihan = unserialize($sr['tagihan_bulanan']);

// 				$this->response['results'][] = $sr;
// 			}
			if( in_array( $pr['bulan_ke'], $this->response['query']['bulan'] ) ) {
				$pr['pembayaran'] = $this->getMasterDetails($pr['id_jenis_pendapatan']);
				$this->response['results'][] = $pr;
			}
		}
		
		return $this->response;
		
		$total_items = $this->tableCount('pembayaran');
		$total_pages = ceil($total_items/$params['limit']);
		$offset = ($params['page']*$params['limit'])-$params['limit'];
		
		$response['total_items'] = $total_items;
		$response['limit'] = $params['limit'];
		$response['page'] = $params['page'];
		$response['offset'] = $params['offset'];
		$response['total_pages'] = $total_pages;
		
		if( count($id_jenis_pendapatan) == 0 ) {
			$jenis_pendapatan = $this->getMaster('jenis_pendapatan');
		} else {
			$selectStatement = $this->pdo->select()
				->from('master')
				->whereIn( 'id', $id_jenis_pendapatan );
			$stmt = $selectStatement->execute();
			$jenis_pendapatan = $stmt->fetchAll();
		}
		
		$ret['query']['jenis_pendapatan'] = $jenis_pendapatan;
		
		if( count($months) == 0 ) {
			$months = $this->months;
		}
		
		if( is_null($tahun) ) {
			$tahun = date('Y');
		}
		$ret['query']['tahun'] = $tahun;
		
		$siswa = $this->getSiswa($id_siswa);
		$ret['query']['siswa'] = $siswa;
		
		$months = array_values($months);
		
// 		for ( $i = 0; $i < count($months); $i++ ) {
			
// 			$ret['results'][$i]['bulan'] = $months[$i];
// 			$ret['results'][$i]['pembayaran'] = array();
			
// 			foreach($jenis_pendapatan as $jp) {
// 				$tagihan = $app->getTanggunganSiswaByMonth( $siswa['id'], $jp['id'], $months[$i] );
				
// 				$ret['results'][$i]['pembayaran'][] = array(
// 					'nama_tagihan'	=> $jp['nama'],
// 					'jumlah'				=> ( ! $tagihan ) ? 0 : $tagihan['jumlah'],
// 					'status'				=> ( ! $tagihan ) ? '' : $app->getMasterNamaTerm($tagihan['status']),
// 					'id_status'			=> ( ! $tagihan ) ? '' : $tagihan['status'],
// 				);

// 			}
// 		}
		
		$selectStatement = $this->pdo->select()
																 ->from('pembayaran')
																 ->where('id_siswa', '=', $id_siswa);
		$stmt = $selectStatement->execute();
		$results = $stmt->fetchAll();
		
		for ( $i = 0; $i < count($results); $i++ ) {
			$pembayaran = $this->getMasterDetails($results[$i]['id_jenis_pendapatan']);
// 			if( false !== $pembayaran ) {
// 				$jumlah = $app->getMasterDetails($pembayaran['id']);
// 				$pembayaran['jumlah'] = ( false !== $jumlah ) ? $jumlah : 0;
// 			}
			$results[$i]['pembayaran'] = $pembayaran;
			$results[$i]['transaksi'] = $app->getTransaksi($results[$i]['id_transaksi']);
		}
		
		$ret['results'] = $results;
		
		return $ret;
	}
	
	public function tagihan() {
		
		$default_params = array(
			'query'								=> array(),
			'id_kelas'						=> 0,
			'pagination'					=> false,
			'page'								=> ( !empty( $_GET['page'] ) ) ? $_GET['page'] : 1,
			'limit'								=> 25,
			'order_by'						=> 'id',
			'order'								=> 'DESC',
			'id_jenis_pendapatan'	=> 0,
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($this->params[$k]) )
				$this->params[$k] = $v;
		}
		$this->response['results'] = array();
		$this->response['query'] = $this->params['query'];		
		
		$selectStatement = $this->pdo->select()
																 ->from('siswa')
																 ->where('id_kelas', '=', $this->params['id_kelas']);
		$stmt = $selectStatement->execute();
		$siswa_results = $stmt->fetchAll();
		
		$results = array();
		$result1 = array();
		foreach( $siswa_results as $sr ) {
			if( null !== $sr['tagihan_bulanan'] ) {
				$tagihan = unserialize($sr['tagihan_bulanan']);
				if( in_array( $this->params['id_jenis_pendapatan'], $tagihan ) ) {
					
					$selectStatement = $this->pdo->select()
						->from('master')
						->whereMany(array(
							'type'	=> 'jumlah_pembayaran',
							'nama'	=> $this->params['id_jenis_pendapatan'],
							'ket'		=> $sr['id_kelas']
						), '=');
					$stmt = $selectStatement->execute();
					$pembayaran = $stmt->fetch();
					if( !$pembayaran ) {
						$pembayaran = $this->getMasterDetails($this->params['id_jenis_pendapatan']);
					} else {
						$jp = $this->getMasterDetails($this->params['id_jenis_pendapatan']);
						$pembayaran['nama'] = $jp['nama'];
					}
					$sr['pembayaran'] = $pembayaran;
					
					$result1[] = $sr;
				}
			}
		}
		
		$months = array_values($this->months);
		for ( $i = 0; $i < count($result1); $i++  ) {
			for ( $j = 0; $j < count($months); $j++ ) {
				$tmp = $result1[$i];
				$tmp['bulan_ke'] = $months[$j];
				$results[] = $tmp;
			}
		}
		
		for ( $i = 0; $i < count($results); $i++  ) {
			$selectStatement = $this->pdo->select()
				->from('pembayaran')
				->whereMany(array(
					'id_siswa'						=> $results[$i]['id'],
					'id_jenis_pendapatan'	=> $this->params['id_jenis_pendapatan'],
					'bulan_ke'						=> $results[$i]['bulan_ke'],
					'tahun'								=> $this->params['tahun'],
				), '=');
			$stmt = $selectStatement->execute();
			$pemb = $stmt->fetch();
			if( false !== $pemb ) {
				unset($results[$i]);
			}
		}
		
		$this->response['results'] = $results;
		return $this->response;
	}
}