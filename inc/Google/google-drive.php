<?php
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$fileIds = $app->get_option('gdrive_filelist_ids');
$fileIds = ( !empty($fileIds) ) ? unserialize($fileIds) : array();

$client = new Google_Client();
$client->setApplicationName("Google Drive Database Backup");
// $client->setDeveloperKey("AIzaSyBF-PcYoLWQVs0ObXrRr8aNR-H2H2W_GjI");
if( file_exists( PATH . '/app/client_credentials.json' ) ) {
	$client->setAuthConfig( PATH . '/app/client_credentials.json' );
} else {
	$data['missing_credentials'] = true;
	$client->setAuthConfig(__DIR__ . '/sample_credentials.json');
}
// $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
// $client->setRedirectUri($redirect_uri);
$client->addScope(Google_Service_Drive::DRIVE);


$service = new Google_Service_Drive($client);

// add "?logout" to the URL to remove a token from the session
if (isset($_REQUEST['logout'])) {
  unset($_SESSION['upload_token']);
}

/************************************************
 * If we have a code back from the OAuth 2.0 flow,
 * we need to exchange that with the
 * Google_Client::fetchAccessTokenWithAuthCode()
 * function. We store the resultant access token
 * bundle in the session, and redirect to ourself.
 ************************************************/
if (isset($_GET['code'])) {
  $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  $client->setAccessToken($token);

  // store in the session also
  $_SESSION['upload_token'] = $token;

  $app->add_flash('success', 'Berhasil terhubung ke Google Drive...');
  $app->redirect('setting.php?target=backup');
}

// set the access token as part of the client
if (!empty($_SESSION['upload_token'])) {
  $client->setAccessToken($_SESSION['upload_token']);
  if ($client->isAccessTokenExpired()) {
    unset($_SESSION['upload_token']);
  }
} else {
  $data['authUrl'] = $client->createAuthUrl();
}

/************************************************
 * If we're signed in then lets try to upload our
 * file. For larger files, see fileupload.php.
 ************************************************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $client->getAccessToken()) {
		
	$folderId = $app->get_option('gdrive_folder_id');
	
	if( empty( $folderId ) ) {
		
		$folder_name = $app->get_option('app_name');
		
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => ( !empty($folder_name) ) ? $folder_name : 'Database Backup',
				'mimeType' => 'application/vnd.google-apps.folder'));
		$file = $service->files->create($fileMetadata, array(
				'fields' => 'id'));
		$folderId = $file->id;
		$app->update_option( 'gdrive_folder_id', $folderId );
		unset($fileMetadata);
		
		$app->update_option( 'gdrive_last_backup', date('Y-m-d') );

	}
	
	
	try {
		
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => 'database-backup-' . date('Y-m-d') . '-' . time(),
				'parents' => array($folderId)
		));
		$content = file_get_contents( PATH . '/uploads/database.sql' );
		$file = $service->files->create($fileMetadata, array(
				'data' => $content,
				'mimeType' => 'application/octet-stream',
				'uploadType' => 'multipart',
				'fields' => 'id'));
		$fileIds[] = $file->id;
		$app->update_option('gdrive_filelist_ids', serialize($fileIds));

		$data['gdrive'] = $file;

		$app->add_flash('success', 'Database berhasil diupload ke Google Drive...');	
		
	} catch(Exception $e) {
		
		$folder_name = $app->get_option('app_name');
		
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => ( !empty($folder_name) ) ? $folder_name : 'Database Backup',
				'mimeType' => 'application/vnd.google-apps.folder'));
		$file = $service->files->create($fileMetadata, array(
				'fields' => 'id'));
		$folderId = $file->id;
		$app->update_option( 'gdrive_folder_id', $folderId );
		unset($fileMetadata);
		
		$app->update_option( 'gdrive_last_backup', date('Y-m-d') );
		
		$fileMetadata = new Google_Service_Drive_DriveFile(array(
				'name' => 'database-backup-' . date('Y-m-d') . '-' . time(),
				'parents' => array($folderId)
		));
		$content = file_get_contents( PATH . '/uploads/database.sql' );
		$file = $service->files->create($fileMetadata, array(
				'data' => $content,
				'mimeType' => 'application/octet-stream',
				'uploadType' => 'multipart',
				'fields' => 'id'));
		$fileIds[] = $file->id;
		$app->update_option('gdrive_filelist_ids', serialize($fileIds));

		$data['gdrive'] = $file;

		$app->add_flash('success', 'Database berhasil diupload ke Google Drive...');	
				
	}
	
}


$data['fileids'] = $fileIds;