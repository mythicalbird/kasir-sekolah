<?php

class AppExtension extends Twig_Extension
{
		protected $pdo;

		public function __construct() {
			$this->pdo = new \Slim\PDO\Database(DSN, USR, PWD);
		}
	
    public function getGlobals()
    {
				global $app;
        return array(
            'modules' => $this->getModules(),
						'request'	=> $_REQUEST,
						'app'			=> $app
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'path', function($slug, $role = null) {
                global $app;
								$module = $app->getModule($slug);
								$url = ( ! $module ) ? $module['url'] : '#';
                return $url;
            }),
            new \Twig_SimpleFunction( 'gravatar', function($email, $atts = array(), $img = true, $s = 80, $d = 'mm', $r = 'g') {
              $url = 'https://www.gravatar.com/avatar/';
              $url .= md5( strtolower( trim( $email ) ) );
              $url .= "?s=$s&d=$d&r=$r";
              if ( $img ) {
                  $url = '<img src="' . $url . '"';
                  foreach ( $atts as $key => $val )
                      $url .= ' ' . $key . '="' . $val . '"';
                  $url .= ' />';
              }
              return $url;
            }),
            new \Twig_SimpleFunction( 'avatar', function($user = null, $atts = array(), $img = true, $s = 80, $d = 'mm', $r = 'g') {
                $url = '';
                if ( null !== $user ) {

                    if ( null !== $user->getImageName() ) {

                        $url = $this->uploader->asset($user, 'imageFile');
                        $url = '<img src="' . $url . '"';
                        foreach ( $atts as $key => $val )
                            $url .= ' ' . $key . '="' . $val . '"';
                            $url .= ' />';

                    } else {

                        $url = 'https://www.gravatar.com/avatar/';
                        $url .= md5( strtolower( trim( $user->getEmail() ) ) );
                        $url .= "?s=$s&d=$d&r=$r";
                        if ( $img ) {
                            $url = '<img src="' . $url . '"';
                            foreach ( $atts as $key => $val )
                                $url .= ' ' . $key . '="' . $val . '"';
                                $url .= ' />';
                        }

                    }

                }
                return $url;
            }),
            new \Twig_SimpleFunction( 'price', function($amount) {
                return 'Rp' . number_format($amount, 0, ',', '.');
            }),
            new \Twig_SimpleFunction( 'option', function($optname) {
                return get_option($optname);
            }),
            new \Twig_SimpleFunction( 'getJumlahSiswa', function($id_kelas) {
            		global $app;
                return $app->tableCount('siswa', 'id_kelas', $id_kelas);
            }),
            new \Twig_SimpleFunction( 'getMasterTerm', function($type, $id=false) {
                global $app;
                return $app->tableCount('siswa', 'id_kelas', $id_kelas);
            }),
            new \Twig_SimpleFunction( 'getTanggunganSiswa', function($id=false) {
                global $app;
                return $app->getTanggunganSiswa($id);
            }),
            new \Twig_SimpleFunction( 'printNamaKas', function($id_kas='') {
								if( $id_kas == '' ) {
									return;
								}
								$selectStatement = $this->pdo->select()
									->from('kas')
									->where('id', '=', $id_kas);
								$stmt = $selectStatement->execute();
								$data = $stmt->fetch();
								return $data['nama_akun'];
            }),
            new \Twig_SimpleFunction( 'printNamaSiswa', function($id_siswa='') {
								if( $id_siswa == '' ) {
									return;
								}
								$selectStatement = $this->pdo->select()
									->from('siswa')
									->where('id', '=', $id_siswa);
								$stmt = $selectStatement->execute();
								$data = $stmt->fetch();
								return $data['nama'];
            }),
            new \Twig_SimpleFunction( 'printNamaKelas', function($id_nama='') {
								if( $id_nama == '' ) {
									return;
								}
								$selectStatement = $this->pdo->select()
									->from('kelas')
									->where('id', '=', $id_nama)
									->orWhere('nama_kelas', '=', $id_nama);
								$stmt = $selectStatement->execute();
								$data = $stmt->fetch();
								return $data['nama_kelas'];
            }),
            new \Twig_SimpleFunction( 'printNamaTerm', function($id_nama='') {
								if( $id_nama == '' ) {
									return;
								}
								$selectStatement = $this->pdo->select()
									->from('master')
									->where('id', '=', $id_nama)
									->orWhere('nama', '=', $id_nama);
								$stmt = $selectStatement->execute();
								$data = $stmt->fetch();
								return $data['nama'];
            }),
            new \Twig_SimpleFunction( 'printNamaUser', function($id_user='') {
								if( $id_user == '' ) {
									return;
								}
								$selectStatement = $this->pdo->select()
									->from('users')
									->where('id', '=', $id_user);
								$stmt = $selectStatement->execute();
								$data = $stmt->fetch();
								return $data['nama'];
            }),
            new \Twig_SimpleFunction( 'printNoTransaksi', function($id_transaksi) {
								global $app;
								$ret = '';
								$transaksi = $app->getTransaksi($id_transaksi);
								if( false !== $transaksi ) {
									$tanggal = $transaksi['tanggal'];
									$ret = '';
									$ret .= date('Y', strtotime($tanggal));
									$ret .= $transaksi['id'];
									$ret = sprintf("%010d", $ret);
								}
								return $ret;
            }),
            new \Twig_SimpleFunction( 'printFlash', function() {
                $flash = $_SESSION['flash'];
								$type = $flash['type'];
								$message = $flash['message'];
                $html = '';
                if ( !empty($type) && !empty($message) ) {
                    $html .= '<script>';
                    $html .= '
                      new PNotify({
													title: "'.ucwords($type).'!",
													text: "'.$message.'",
													type: "'.$type.'"
                      });
                    ';
                    $html .= '</script>';
                }
								unset($_SESSION['flash']);
                return $html;
            }),
            new \Twig_SimpleFunction( 'pagination', function($total = 0, $current = 1) {
							
								$get_queries = $_GET;
								$uri = basename($_SERVER['PHP_SELF']);
							
								if( !empty( $_GET['page'] ) ) {
									$current = $_GET['page'];
								}
								
								$html = '';
								$html .= '
									<nav aria-label="Page navigation">
										<ul class="pagination">
											<li>
												<a href="#" aria-label="Previous">
													<span aria-hidden="true">&laquo;</span>
												</a>
											</li>
								';
								for( $i = 1; $i < $total+1; $i++ ) {
									$queries = array_merge($get_queries, array(
										'page' => $i
									));
									$url = $uri . '?' . http_build_query($queries);
									$html .= '<li class="';
									$html .= ( $i == $current ) ? 'active' : '';
									$html .= '"><a href="'.$url.'">'.$i.'</a></li>';
								}
								$html .= '
											<li>
												<a href="#" aria-label="Next">
													<span aria-hidden="true">&raquo;</span>
												</a>
											</li>
										</ul>
									</nav>
								';
								return $html;
            }),
        );
    }

    public function getFilters()
    {
        return array(
            // new Twig_Filter('rot13', 'str_rot13'),
        );
    }
	
	
		private function getModules()
		{
				$selectStatement = $this->pdo->select()
																		 ->from('modules');
				$stmt = $selectStatement->execute();
				$data = $stmt->fetchAll();
			
				return $data;
		}
}