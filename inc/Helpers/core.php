<?php

/*=======================
 * COMMON FUNCTIONS
 =======================*/

function get_uploaded_images($dir_path) {
	$arr = scandir($dir_path);
	unset($arr[0]);
	unset($arr[1]);
	$data = array_merge($arr, array());
	
	return $data;
}

function mwt_current_user() {
	$is_logged = MWT::isSignedIn();
	
	if( $is_logged === true && isset($_SESSION['userId']) ) {
		return get_user($_SESSION['userId']);
	} else {
		return FALSE;
	}

}

function mwt_gravatar( $email, $atts = array(), $img = true, $s = 80, $d = 'mm', $r = 'g' ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

function mwt_upload( $file_input_name ) {
	$uploaded = false;
	if( isset($_FILES[$file_input_name]['tmp_name']) ) {
		
		$upload_dir = MWT_ROOT . '/uploads/';
		$image_name = date("YmdHis") . '_' . $_FILES[$file_input_name]['name'];
		$image_size = $_FILES[$file_input_name]['size'];
		// do upload
		$upload = move_uploaded_file($_FILES[$file_input_name]['tmp_name'], $upload_dir . $image_name);
		
		if($upload) {
			global $db_prefix;
			$column = array('type', 'title', 'slug');
			$values = array('media', $image_name, strtolower(str_replace(' ', '-', $image_name)));
			$insertId = insert($db_prefix . 'posts', $column, $values, false);
			
			$uploaded = array(
				'image_name'	=> $image_name,
			);
		}
		
	}
	
	return $uploaded;

}

function mwt_currency_exchange_rates( $to = 'IDR', $from = 'USD' ) {
	// set API Endpoint and access key (and any options of your choice)
	$endpoint = 'live';
	$access_key = CURRENCY_LAYER_API_KEY;

	// Initialize CURL:
	$ch = curl_init('http://apilayer.net/api/'.$endpoint.'?access_key='.$access_key.'');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Store the data:
	$json = curl_exec($ch);
	curl_close($ch);

	// Decode JSON response:
	$exchangeRates = json_decode($json, true);

	return $exchangeRates['quotes'][$from . $to];
}

// calculate due date
function mwt_duedate($strtotime, $format = "d-m-Y") {
	$daydue = strtotime($strtotime);
	$duedate = date($format, $daydue);
	return $duedate;
}

// calculate due date by cycle
function mwt_duedate_by_cycle($cycle, $format = "Y-m-d") {
	$cycles = ['monthly', 'quarterly', 'semiannually', 'annually', 'biennially', 'triennially'];
	if(in_array($cycle, $cycles)) {
		if($cycle == "monthly") { $daydue = "+37 day"; }
		elseif($cycle == "quarterly") { $daydue = "+97 day"; }
		elseif($cycle == "semiannually") { $daydue = "+187 day"; }
		elseif($cycle == "annually") { $daydue = "+372 day"; }
		elseif($cycle == "biennially") { $daydue = "+737 day"; }
		elseif($cycle == "triennially") { $daydue = "+1102 day"; }
		$daydue = strtotime($daydue);
		$duedate = date($format, $daydue);
		return $duedate;
	} else {
		return FALSE;	
	}
}

// list negara
function mwt_countries($logged=false) {
	$countries = array(
		'US - United States',
		'AF - Afghanistan',
		'AL - Albania',
		'DZ - Algeria',
		'AS - American Samoa',
		'AD - Andorra',
		'AO - Angola',
		'AI - Anguilla',
		'AQ - Antarctica',
		'AG - Antigua and Barbuda',
		'AR - Argentina',
		'AM - Armenia',
		'AW - Aruba',
		'AU - Australia',
		'AT - Austria',
		'AZ - Azerbaijan',
		'BS - Bahamas',
		'BH - Bahrain',
		'BD - Bangladesh',
		'BB - Barbados',
		'BY - Belarus',
		'BE - Belgium',
		'BZ - Belize',
		'BJ - Benin',
		'BM - Bermuda',
		'BT - Bhutan',
		'BO - Bolivia',
		'BA - Bosnia and Herzegovina',
		'BW - Botswana',
		'BR - Brazil',
		'BN - Brunei',
		'BG - Bulgaria',
		'BF - Burkina Faso',
		'BI - Burundi',
		'KH - Cambodia',
		'CM - Cameroon',
		'CA - Canada',
		'CV - Cape Verde',
		'KY - Cayman Islands',
		'CF - Central African Republic',
		'TD - Chad',
		'CL - Chile',
		'CN - China',
		'CX - Christmas Island',
		'CC - Cocos (Keeling) Islands',
		'CO - Colombia',
		'KM - Comoros',
		'CG - Congo - Brazzaville',
		'CD - Congo - Kinshasa',
		'CK - Cook Islands',
		'CR - Costa Rica',
		'CI - Cote D\'Ivoire',
		'HR - Croatia',
		'CU - Cuba',
		'CY - Cyprus',
		'CZ - Czech Republic',
		'DK - Denmark',
		'DJ - Djibouti',
		'DM - Dominica',
		'DO - Dominican Republic',
		'TP - East Timor',
		'EC - Ecuador',
		'EG - Egypt',
		'SV - El Salvador',
		'GQ - Equatorial Guinea',
		'ER - Eritrea',
		'EE - Estonia',
		'ET - Ethiopia',
		'FO - Faroe Islands',
		'FJ - Fiji',
		'FI - Finland',
		'FR - France',
		'GF - French Guiana',
		'PF - French Polynesia',
		'GA - Gabon',
		'GB - United Kingdom',
		'GM - Gambia',
		'GE - Georgia',
		'DE - Germany',
		'GH - Ghana',
		'GI - Gibraltar',
		'GR - Greece',
		'GL - Greenland',
		'GD - Grenada',
		'GP - Guadeloupe',
		'GU - Guam',
		'GT - Guatemala',
		'GN - Guinea',
		'GW - Guinea-Bissau',
		'GY - Guyana',
		'HT - Haiti',
		'EL - Hellenic Republic (Greece)',
		'HN - Honduras',
		'HK - Hong Kong',
		'HU - Hungary',
		'IS - Iceland',
		'IN - India',
		'ID - Indonesia',
		'IR - Iran',
		'IQ - Iraq',
		'IE - Ireland',
		'IL - Israel',
		'IT - Italy',
		'JM - Jamaica',
		'JP - Japan',
		'JO - Jordan',
		'KZ - Kazakhstan',
		'KE - Kenya',
		'KI - Kiribati',
		'KW - Kuwait',
		'KG - Kyrgyzstan',
		'LA - Laos',
		'LV - Latvia',
		'LB - Lebanon',
		'LS - Lesotho',
		'LR - Liberia',
		'LY - Libya',
		'LI - Liechtenstein',
		'LT - Lithuania',
		'LU - Luxembourg',
		'MO - Macau',
		'MK - Macedonia',
		'MG - Madagascar',
		'MW - Malawi',
		'MY - Malaysia',
		'MV - Maldives',
		'ML - Mali',
		'MT - Malta',
		'MH - Marshall Islands',
		'MQ - Martinique',
		'MR - Mauritania',
		'MU - Mauritius',
		'YT - Mayotte',
		'MX - Mexico',
		'MD - Moldova',
		'MC - Monaco',
		'MN - Mongolia',
		'ME - Montenegro',
		'MS - Montserrat',
		'MA - Morocco',
		'MZ - Mozambique',
		'MM - Myanmar (Burma)',
		'NA - Namibia',
		'NR - Nauru',
		'NP - Nepal',
		'NL - Netherlands',
		'AN - Netherlands Antilles',
		'NC - New Caledonia',
		'NZ - New Zealand',
		'NI - Nicaragua',
		'NE - Niger',
		'NG - Nigeria',
		'NU - Niue',
		'NF - Norfolk Island',
		'MP - Northern Mariana Islands',
		'NO - Norway',
		'OM - Oman',
		'PK - Pakistan',
		'PW - Palau',
		'PA - Panama',
		'PG - Papua New Guinea',
		'PY - Paraguay',
		'PE - Peru',
		'PH - Philippines',
		'PN - Pitcairn Islands',
		'PL - Poland',
		'PT - Portugal',
		'PR - Puerto Rico',
		'QA - Qatar',
		'RE - Reunion',
		'RO - Romania',
		'RU - Russia',
		'RW - Rwanda',
		'KN - Saint Kitts And Nevis',
		'LC - Saint Lucia',
		'WS - Samoa',
		'SM - San Marino',
		'ST - Sao Tome And Principe',
		'SA - Saudi Arabia',
		'SN - Senegal',
		'RS - Serbia',
		'SC - Seychelles',
		'SL - Sierra Leone',
		'SG - Singapore',
		'SK - Slovakia',
		'SI - Slovenia',
		'SB - Solomon Islands',
		'SO - Somalia',
		'ZA - South Africa',
		'KR - South Korea',
		'ES - Spain',
		'LK - Sri Lanka',
		'SH - St. Helena',
		'PM - St. Pierre And Miquelon',
		'SD - Sudan',
		'SR - Suriname',
		'SZ - Swaziland',
		'SE - Sweden',
		'CH - Switzerland',
		'SY - Syria',
		'TW - Taiwan',
		'TJ - Tajikistan',
		'TZ - Tanzania',
		'TH - Thailand',
		'TG - Togo',
		'TK - Tokelau',
		'TO - Tonga',
		'TT - Trinidad and Tobago',
		'TN - Tunisia',
		'TR - Turkey',
		'TM - Turkmenistan',
		'TC - Turks and Caicos Islands',
		'TV - Tuvalu',
		'UG - Uganda',
		'UA - Ukraine',
		'AE - United Arab Emirates',
		'UY - Uruguay',
		'UZ - Uzbekistan',
		'VU - Vanuatu',
		'VA - Vatican City',
		'VE - Venezuela',
		'VN - Vietnam',
		'VI - Virgin Islands (U.S.)',
		'EH - Western Sahara',
		'YE - Yemen',
		'ZM - Zambia',
		'ZW - Zimbabwe'
	);
		
	if ($logged == true) {
		for($i = 0; $i < count($countries); $i++)
			{
				if (substr($countries[$i],5) == 'Indonesia') {
					echo '<option value="'. substr($countries[$i],5) .'" selected>' . $countries[$i] . '</option>';	
				} else {
					echo '<option value="'. substr($countries[$i],5) .'">' . $countries[$i] . '</option>';	
				}
			}
	} else {
		return $countries;
	}

}

function mwt_generate_token($length = 78) {
	//Generate a random string.
	$token = openssl_random_pseudo_bytes($length);

	//Convert the binary data into hexadecimal representation.
	$token = bin2hex($token);
	return $token;
}

function mwt_cleanUrl($string, $space = false, $delimeter = '-') {
	if($space !== false) {
		$string = str_replace(' ', $delimeter, $string);	
	}
	
	return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
} 

function mwt_generate_password() {
    $alphabet = '!@#$%^&*()_+abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 99; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return substr(implode($pass),rand(9,99),15); //turn the array into a string
}

function mwt_generate_slug($param, $table, $key = 'slug') {
	$slugs = distinct($table, $key);

	$tmp_slug = strtolower(mwt_cleanUrl($param, true));
	if (in_array($tmp_slug, $slugs)) {
		$slug = $tmp_slug . '-' . date('His');
	} else {
		$slug = $tmp_slug;
	}

	return $slug;
}

// Automatically Add Paragraph
function mwt_nl2p($str) {
    $arr=explode("\n",$str);
    $out='';

    for($i=0;$i<count($arr);$i++) {
        if(strlen(trim($arr[$i]))>0)
            $out.='<p>'.trim($arr[$i]).'</p>';
    }
    return $out;
}