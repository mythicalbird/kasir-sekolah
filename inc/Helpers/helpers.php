<?php

$pdo = new \Slim\PDO\Database(DSN, USR, PWD);


function get_option($opt_name) {

  global $pdo;
 
  $selectStatement = $pdo->select(['option_value'])
                         ->from( 'options' )
												 ->where('option_name', '=', $opt_name);
  $stmt = $selectStatement->execute();
  $data = $stmt->fetch();

  return $data['option_value'];

}


/*********************************
 *
 * Manipulasi DB dan PROSES/AKSI
 *
 ********************************/

function distinct($table, $column) {
  global $pdo;

  $selectDistinct = $pdo->select([$column])
                   ->distinct()
                   ->from($table);
  $stmt = $selectDistinct->execute();
  $results = $stmt->fetchAll();

  $data = array();
  foreach( $results as $result ) {
    $data[] = $result[$column];
  }

    return $data;
}

/**
 * @param array
 * @return boolean
 *
 */
function insert($table, $column, $values, $insertId = false) {
  global $pdo;
  
  $insertStatement = $pdo->insert($column)
                        ->into($table)
                        ->values($values);
  $affectedRows = $insertStatement->execute($insertId);

  return $affectedRows;
}

/**
 * @param array
 * @return boolean
 */
function update($table, $param = array(), $opt = array()) {
  global $pdo, $db_prefix;
  
  $updateStatement = $pdo->update($param)
                        ->table($table)
                        ->where($opt[0], $opt[1], $opt[2]);
  $affectedRows = $updateStatement->execute();
  
  return $affectedRows;
}

/**
 * @param array
 * @return boolean
 */
function updateMany($table, $param1, $param2, $opt=array()) {
  global $pdo, $db_prefix;

  $updateStatement = $pdo->update($param1)
                         ->set($param2)
                         ->table($table)
                         ->where($opt[0], $opt[1], $opt[2]);
  $affectedRows = $updateStatement->execute();

  return $affectedRows;
}

function delete($table, $where) {
  global $pdo, $db_prefix;

  $deleteStatement = $pdo->delete()
                        ->from($table)
                        ->where($where[0], $where[1], $where[2]);
  $affectedRows = $deleteStatement->execute();

  return $affectedRows;
    
}