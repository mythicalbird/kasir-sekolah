<?php

class Mwt
{

	protected $pdo;
	public $user;
	public $response = array();

	public function __construct() {
		$this->pdo = new \Slim\PDO\Database(DSN, USR, PWD);
		$this->user = $this->getUser( $_SESSION['_user'] );
	}
	
	public static function signOut( $redirect_url ) {
		unset($_SESSION['_islogged']); unset($_SESSION['_user']);
		session_unset();
		session_destroy();
		header( 'location:' . $redirect_url );
		exit;
	}
	
	public static function generate_token($length = 32) {

		//Generate a random string.
		$token = openssl_random_pseudo_bytes($length);

		//Convert the binary data into hexadecimal representation.
		$token = bin2hex($token);
		return $token;

	}

	public static function debug( $var, $exit= true ) {
		echo '<pre>'; var_dump($var); echo '</pre>';
		if($exit) exit;
	}
	
	public function tableCount($table, $column = '', $value = '', $compare = '=') {
		$selectStatement = $this->pdo->select()
			->from($table);
		if( $column != '' && $value != '' ) {
			$selectStatement->where($column, $compare, $value);
		}
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		return count($data);
	}

	public function signUp($params, $autologin = false) {

		if ( false !== $this->getUser($params['email']) ) {

			$response = array(
				'success'	=> 0,
				'error'		=> 'Email sudah terdaftar',
			);

		} elseif ( false !== $this->getUser($params['username']) ) {

			$response = array(
				'success'	=> 0,
				'error'		=> 'Username sudah terdaftar',
			);

		} elseif ( false !== $this->getUser($params['no_identitas']) ) {

			$response = array(
				'success'	=> 0,
				'error'		=> 'No. identitas tidak bisa digunakan.',
			);

		} else {

			$id_user = $this->inputUser($params);

			if ( $autologin !== false ) {
				$this->signIn('user', $params['email'], $params['password'], 1);
			}

			$response = array(
				'success'	=> 1,
				'id_user'	=> $id_user,
				'error'		=> '',
			);

		}

		// kirim email notifikasi pendaftaran sukses
		$subject = 'Konfirmasi Pendaftaran';
		$message = "
			<p>Halo {$params['nama']},</p>
			
			<p>Email ini adalah pemberitahuan bahwa pendaftaran anda untuk mengikuti Tryout telah berhasil.</p>
			
			<p>Informasi akun:</p>
			
			<p>
				Username: <strong>{$params['username']}</strong><br />
				Kata sandi: <em>{$params['password']}</em>
			</p>
			
			<p>Untuk login dan mengikuti Tryout, silahkan klik link dibawah ini atau salin dan tempelkan di tab baru pada browser anda.</p>
			
			<p><a href=\"http://".$_SERVER['HTTP_HOST']."/login.php\">http://".$_SERVER['HTTP_HOST']."/login.php</a></p>
			
			<br />
			
			<p>Terima kasih.</p>
		";
		$this->sendmail($params['email'], $subject, $message);
		
		return $response;
	
	}
	
	public function signIn($login, $password, $rememberme = 0) {

		$user = $this->getUser($login);

		if( false === $user ) {
			$this->response = array(
				'success'	=> 0,
				'message'	=> 'Username/password salah...',
			);
		} else {
			
				if( password_verify($password, $user['password']) ) {

					if( $rememberme === 0 ) {
						$_SESSION['_islogged'] = true;
						$_SESSION['_user'] = $user['id'];
					} else {
						$_SESSION['_islogged'] = true;
						$_SESSION['_user'] = $user['id'];
					}

					// update user
					$params = array(
						'last_login'	=> date('Y-m-d H:i:s'),
						'sid'					=> session_id()
					);
					$this->updateOrInsert(
						'users',
						$params,
						$user['id']
					);

					$this->response = array(
						'success'	=> 1,
						'message'	=> '',
					);

				} else {
					
					$this->response = array(
						'success'	=> 0,
						'message'	=> 'Username/password salah...',
					);
					
				}
			
		}

		return $this->response;

	}
	
	public function requestPw($email) {
		$selectStatement = $this->pdo->select()
									 ->from('users')
									 ->where('email', '=', $email);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();

		if ( false !== $data ) {

			$token = $this->generate_token();
			$_SESSION['token'] = $token;
			$_SESSION['email'] = $email;
			
			$reset_url = 'http://' . $_SERVER['HTTP_HOST'] . '/login.php?action=resetpw&email=' . $email . '&token=' . $token;
			
			$message = '
			<p>Halo</p>
			
			<p>Anda baru saja melakukan permintaan atur ulang kata sandi. Untuk membuat kata sandi baru, silahkan klik link dibawah ini:</p>
			
			<br />
			
			<p><a href="'.$reset_url.'">'.$reset_url.'</a></p>
			
			<br /><br />

			<p>Terima kasih</p>
			';
			
			$this->sendmail($email, 'Permintaan kata sandi baru', $message);

		}

	}

	public function getListUser($params) {
		
		$default_params = array(
			'level'				=> 0,
			'pagination'	=> false,
			'page'				=> ( !empty( $_GET['page'] ) ) ? $_GET['page'] : 1,
			'limit'				=> 25,
			'order_by'		=> 'id',
			'order'				=> 'DESC',
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($params[$k]) )
				$params[$k] = $v;
		}
		
		$total_items = $this->tableCount('users');
		$total_pages = ceil($total_items/$params['limit']);
		$offset = ($params['page']*$params['limit'])-$params['limit'];
		
		$this->response['total_items'] = $total_items;
		$this->response['limit'] = $params['limit'];
		
		$this->response['page'] = $params['page'];
		$this->response['offset'] = $params['offset'];
		$this->response['total_pages'] = $total_pages;

		$selectStatement = $this->pdo->select()
																 ->from('users');
												
		if( (int)$params['level'] > 0 )	 {
			$selectStatement->where('id_level', '=', $params['level']);
		} 
		
		if( false !== $params['pagination'] ) {
			$selectStatement->limit( $params['limit'], $offset );
		}
		
		if( $params['order_by'] != '' ) {
			$selectStatement->orderBy( $params['order_by'], $params['order'] );
		}
		
		$stmt = $selectStatement->execute();
		$this->response['results'] = $stmt->fetchAll();

		for ( $i = 0; $i < count($this->response['results']); $i++  ) {
			$this->response['results'][$i]['level'] = $this->getMasterNamaTerm($result[$i]['id_level']);
		}
		return $this->response;

	}

	public function getUser($login) {
		$selectStatement = $this->pdo->select()
			->from('users')
			->where('id', '=', $login)
			//->orWhere('email', '=', $login)
			->orWhere('username', '=', $login);
		$stmt = $selectStatement->execute();
		$this->response['results'] = $stmt->fetch();
		if( false !== $this->response['results'] ) {
			$this->response['results']['roles'] = array(
				'ROLE_' . strtoupper( $this->slugify( $this->getMasterNamaTerm($this->response['results']['id_level']) ) )
			);
		}
		return $this->response['results'];
	}

	public function getModule($slug_id) {
		$selectStatement = $this->pdo
			->select()
			->from('modules')
			->where('id', '=', $slug_id)
			->orWhere('slug', '=', $slug_id)
			->orWhere('url', '=', $slug_id);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		return $data;
	}
	
	public function load($template, $params = array()) 
	{
		global $twig;
		
// 		if ( "anon." != $this->token_storage->getToken()->getUser() ) {
			$_role = $this->user['roles'];
			$_role = $_role[0];
		
// 			if( $_role != 'ROLE_SUPER_ADMIN' ) :

// 				$module = $this->getModule( basename( $_SERVER['REQUEST_URI'] ) );
				$selectStatement = $this->pdo
					->select()
					->from('modules')
					->whereLike('url', '%'.basename( $_SERVER['PHP_SELF'] ).'%');
				$stmt = $selectStatement->execute();
				$module = $stmt->fetch();
		
				if( !$module ) {
					http_response_code(404);
					$template = "default/404.html.twig";
					return $twig->render( $template, $params );
				}
		
				if ( null !== $module['parent_id'] ) {

					$parent = $this->getModule($module['parent_id']);

					if ( !in_array( $_role, unserialize($parent['roles']) ) ) {
						http_response_code(404);
						$template = "default/404.html.twig";
						return $twig->render( $template, $params );
					}

				} 
		
				if ( !in_array( $_role, unserialize($parent['roles']) ) ) {
					http_response_code(404);
					$template = "default/404.html.twig";
					return $twig->render( $template, $params );
				}
		
// 			endif;
		
// 		}
		
		return $twig->render( $template, $params );
	}
	
	public function slugify($str, $sep = '_') 
	{
		$slug = preg_replace('/[^A-Za-z0-9\-]/', '', $str);
      	$slug = str_replace(' ', $sep, strtolower($str));
      	return $slug;
	}
	
	public function getInfoKas($id_kas) {

		$selectStatement = $this->pdo->select()
																 ->from('kas')
																 ->where('id', '=', $id_kas);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		return $data;
	}

	public function getListKas($jenis=false) {

		$selectStatement = $this->pdo->select()
																 ->from('kas');
		if( false !== $jenis ) {
			$selectStatement->where('jenis', '=', $jenis);
		}
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function updateUserPw($userid, $new_plain_text_password) {
		$pass = $this->encryptpw($new_plain_text_password);

		$update = update('users', array('password' => $pass), array('ID', '=', $userid));

		return $update;
	}
	
 	public function encode_pw( $plain_text ) {
		$option = [
			'cost'	=> 11
		];
		$hashed_pass = password_hash($plain_text, PASSWORD_BCRYPT, $option);
		return $hashed_pass;	
  }
	
	public function getFilterListPembayaran($params) {

		$selectStatement = $this->pdo->select()
			->from('pembayaran')
			->where('id_siswa', '=', $params['id_siswa'], 'AND')
			->where('id_jenis_pendapatan', '=', $params['id_jenis_pendapatan']);
		$stmt = $selectStatement->execute();
		$result = $stmt->fetchAll();
		
		for ( $i = 0; $i < count($result); $i++  ) {
			$result[$i]['siswa'] = $this->getSiswa($result[$i]['id_siswa']);
		}

		return $result;
	}
	
	public function getListPembayaran( $params=array() ) {
		
		$default_params = array(
			'query'								=> array(),
			'id_siswa'						=> 0,
			'pagination'					=> false,
			'page'								=> ( !empty( $_GET['page'] ) ) ? $_GET['page'] : 1,
			'limit'								=> 25,
			'order_by'						=> 'id',
			'order'								=> 'DESC',
			'id_jenis_pendapatan'	=> 0,
		);
		foreach( $default_params as $k => $v ) {
			if( !isset($params[$k]) )
				$params[$k] = $v;
		}
		$this->response['query'] = $params['query'];
		
		$total_items = $this->tableCount('pembayaran');
		$total_pages = ceil($total_items/$params['limit']);
		$offset = ($params['page']*$params['limit'])-$params['limit'];
		
		$this->response['total_items'] = $total_items;
		$this->response['limit'] = $params['limit'];
		
		$this->response['page'] = $params['page'];
		$this->response['offset'] = $params['offset'];
		$this->response['total_pages'] = $total_pages;

		$selectStatement = $this->pdo->select()
																 ->from('pembayaran');
												
		if( (int)$params['id_siswa'] > 0 )	 {
			if( $params['id_jenis_pendapatan'] != '' ) {
				$selectStatement->where('id_siswa', '=', $params['id_siswa'], 'AND')
					->where('id_jenis_pendapatan', '=', $params['id_jenis_pendapatan']);
			} else {
				$selectStatement->where('id_siswa', '=', $params['id_siswa']);
			}
		} 
		
		if( false !== $params['pagination'] ) {
			$selectStatement->limit( $params['limit'], $offset );
		}
		
		if( $params['order_by'] != '' ) {
			$selectStatement->orderBy( $params['order_by'], $params['order'] );
		}
		
		$stmt = $selectStatement->execute();
		$results = $stmt->fetchAll();

		for ( $i = 0; $i < count($results); $i++  ) {
			$results[$i]['siswa'] = $this->getSiswa($results[$i]['id_siswa']);
			$results[$i]['transaksi'] = $this->getTransaksi($results[$i]['id_transaksi']);
		}
		$this->response['results'] = $results;
		return $this->response;
	}
	
	public function getListPengeluaran($id_siswa=false) {

		$selectStatement = $this->pdo->select()
																 ->from('pengeluaran');
												
		if( false !== $id_siswa )	 {
			$selectStatement->where('id_siswa', '=', $id_siswa);
		}
		$stmt = $selectStatement->execute();
		$result = $stmt->fetchAll();

		return $result;
	}
	
	/**
	 * @param 'pembayaran' atau 'pengeluaran' = nama_table
	 * @param $id = id dari pembayaran atau pengeluaran
	 */
	public function getIdTransaksi($jenis, $id = 0) {
		if( $id == 0 ) {
			return 0;
		}
		$selectStatement = $this->pdo->select()
			->from($jenis)
			->where('id', '=', $id);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		return ( ! $data ) ? 0 : $data['id'];
	}
	
	public function getTransaksi($id) {
		$selectStatement = $this->pdo->select()
			->from('transaksi')
			->where('id', '=', $id);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		return $data;
	}

	public function get_option($opt_name) {

		$selectStatement = $this->pdo->select(['option_value'])
			->from( 'options' )
			->where('option_name', '=', $opt_name);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();

		if( ! $data ) {
			$insertStatement = $this->pdo
				->insert(array('option_name', 'option_value'))
				->into('options')
				->values(array($opt_name, ''));
			$insertStatement->execute(false);
			return;
		}
		
		return $data['option_value'];

	}
	
	public static function update_option($optname, $optvalue) {

		if ( null !== get_option($optname) ) {
			$result = update('options', array('option_value' => $optvalue), array('option_name', '=', $optname));
		} else {
			$result = insert('options', array('option_name', 'option_value'), array($optname, $optvalue), true);
		}
		return $result;
	}
	
	/**
	 * Kirim email
	 */
	public function sendmail( $to, $subject, $message ) {
		
		$from = ( !empty(get_option('app_admin_email')) ) ? get_option('app_admin_email') : 'noreply@' . $_SERVER['HTTP_HOST'];

		// To send HTML mail, the Content-type header must be set
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=iso-8859-1';

		// Additional headers
		$headers[] = 'To: ' . $to;
		$headers[] = 'From: ' . get_option('app_name') . ' <' . $from . '>';

		// Mail it
		$send = mail($to, $subject, $message, implode("\r\n", $headers));
		
		return $send;
		
	}

	protected function insert_meta( $params = array() ) {

		$column = array('relid', 'meta_name', 'meta_value');
		$values = array($params[0], $params[1], $params[2]);

		$insertId = insert('metas', $column, $values);

		return $insertId;

	}

	public function getAdminStat() {
		$result = array();
		// user
		$selectStatement = $this->pdo->select(['ID'])
									 ->from('users');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$result['users'] = array(
			'count' => count($data)
			);

		// paket_soal
		$selectStatement = $this->pdo->select(['id'])
									 ->from('paket_soal');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$result['paket_soal'] = array(
			'count' => count($data)
			);

		// soal
		$selectStatement = $this->pdo->select(['id_soal'])
									 ->from('soal');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$result['soal'] = array(
			'count' => count($data)
			);

		// admin
		$selectStatement = $this->pdo->select(['ID'])
									 ->from('admins');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$result['admins'] = array(
			'count' => count($data)
			);

		return $result;
	}



	/******************************/

	public function getMaster($term='') {
		if ( $term != '' ) {
			$selectStatement = $this->pdo->select()
										 ->from('master')
										 ->where('type', '=', $term);
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('master');
		}
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function getMasterDetails($id_term) {
		$selectStatement = $this->pdo->select()
									 ->from('master')
									 ->where('id', '=', $id_term);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		

		
		return $data;
	}

	public function updateOrInsert($table, $params = array(), $key_value = 0, $key_name = 'id', $key_compare = '=') {
		$result = array(
			'id'		=> $key_value,
			'success'	=> false,
			'insert'	=> false,
		);
		$selectStatement = $this->pdo->select()
									 ->from($table)
									 ->where($key_name, $key_compare, $key_value);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();

		if ( count($params) > 1 ) {

			if ( ! $data ) {

				$result['insert'] = true;

				$column = array_keys($params);
				$values = array_values($params);
				$result['id'] = insert($table, $column, $values, true);

			} else {

				$result['success'] = updateMany(
					$table, 
					array($key_name => $key_value), 
					$params, 
					array($key_name, $key_compare, $key_value)
				);

			}

		}


		return $result;
	}

	public function getSiswa($id_siswa=false) {
		if ( $id_siswa !== false ) {
			$selectStatement = $this->pdo->select()
										 ->from('siswa')
										 ->where('id', '=', $id_siswa);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetch();
			if( $data ) {
				$data['kelas'] = $this->getKelas($data['id_kelas']);
			}
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('siswa');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();

			for ( $i = 0; $i < count($data); $i++  ) {
				$data[$i]['kelas'] = $this->getKelas($data[$i]['id_kelas']);
			}
			
		}

		return $data;
	}
	
	public function getListSiswa($id_kelas='') {
		
		if ( $id_kelas != '' ) {
			$selectStatement = $this->pdo->select()
										 ->from('siswa')
										 ->where('id_kelas', '=', $id_kelas);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('siswa');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		}
		
		for ( $i = 0; $i < count($data); $i++  ) {
			$data[$i]['kelas'] = $this->getKelas($data[$i]['id_kelas']);
		}
		
		$this->response['total_items'] = count($data);
		$this->response['results'] = array();
		foreach( $data as $row ) {
			$tagihan_bulanan = array();
			if( null !== $row['tagihan_bulanan'] ) {
				$row['tagihan_bulanan'] = unserialize($row['tagihan_bulanan']);
				foreach( $row['tagihan_bulanan'] as $id_jenis_pendapatan ) {
					$tagihan_bulanan[] = $this->getMasterDetails($id_jenis_pendapatan);
				}
			} else {
				$row['tagihan_bulanan'] = array();
			}
			$row['tanggungan'] = $tagihan_bulanan;
			$this->response['results'][] = $row;
		}

		return $this->response;
	}

	public function getKelas($id_kelas=false) {
		if ( $id_kelas !== false ) {
			$selectStatement = $this->pdo->select()
				->from('kelas')
				->where('id', '=', $id_kelas)
				->orWhere('nama_kelas', '=', $id_kelas);;
			$stmt = $selectStatement->execute();
			$data = $stmt->fetch();
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('kelas');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		}

		return $data;
	}
	
	public function getListKelas() {
		$selectStatement = $this->pdo->select()
									 ->from('kelas');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
		$this->response['total_items'] = count($data);
		$this->response['results'] = $data;
		return $this->response;
	}
	
	public function getPembayaran($id_pembayaran=false) {
		if ( $id_pembayaran !== false ) {
			$selectStatement = $this->pdo->select()
										 ->from('pembayaran')
										 ->where('id', '=', $id_pembayaran);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetch();
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('pembayaran');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		}

		return $data;
	}
	
	public function getPengeluaran($id_pengeluaran=false) {
		if ( $id_pembayaran !== false ) {
			$selectStatement = $this->pdo->select()
										 ->from('pengeluaran')
										 ->where('id', '=', $id_pengeluaran);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetch();
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('pengeluaran');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		}

		return $data;
	}

	public function getJumlahSiswaPerKelas($id_kelas=false) {
		if ( $id_kelas !== false ) {
			$selectStatement = $this->pdo->select()
										 ->from('kelas')
										 ->where('id', '=', $id_kelas);
			$stmt = $selectStatement->execute();
			$data = $stmt->fetch();
		} else {
			$selectStatement = $this->pdo->select()
										 ->from('kelas');
			$stmt = $selectStatement->execute();
			$data = $stmt->fetchAll();
		}

		return $data;
	}


	public function dumpSql() {
		global $config;
		$filePath = PATH . '/uploads/database.sql';
		$command = 'mysqldump';
		$command .= ' --user=';
		$command .= $config['db']['user'];
		$command .= ' --password=';
		$command .= $config['db']['password'];
		$command .= ' --host=localhost ';
		$command .= $config['db']['name'];
		$command .= ' > ' . $path . '/' . $filePath;
		$exec = exec($command);
	}


	public function getMasterNamaTerm($id_term) {
		$selectStatement = $this->pdo->select()
									 ->from('master')
									 ->where('id', '=', $id_term);
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		if ( ! $data ) {
			return;
		}
		return $data['nama'];
	}


	public function redirect($url, $code = 302) {
		header('Location:' . $url, $code);
		exit;
	}

	public function add_flash($type = '', $message = '') {
		$_SESSION['flash'] = array(
			'type'	=> $type,
			'message' => $message
		);
	}
	
	public function excelImport($table, $data = array()) {
		
		if( $table != '' ) :
			foreach( $data as $row ) {

				$kelas = $this->getKelas($row[2]);
				$id_kelas = ( false !== $kelas ) ? $kelas['id'] : null;
				$params = array(
					'nis'					=> $row[0],
					'nama'      	=> $row[1],
					'id_kelas'    => $id_kelas,
					'tpt_lahir'   => $row[3],
					'tgl_lahir'   => ( !empty( $row[4] ) ) ? date('Y-m-d', strtotime($row[4])) : null,
					'jk'      		=> $row[5],
					'agama'     	=> $row[6],
					'email'     	=> $row[7],
					'no_hp'     	=> $row[8],
					'alamat'    	=> $row[9],
					'kota'      	=> $row[10],
					'provinsi'    => $row[11],
					'kodepos'   	=> $row[12],
					'created_at' 	=> date('Y-m-d H:i:s'),
					'updated_at'  => date('Y-m-d H:i:s'),
				);

				$this->updateOrInsert( 
						$table, 
						$params,
						$row[0],
						'nis'
					);			
			}
		endif;
		
		return;
	}
	
	public function getTanggunganSiswa($id_siswa, $tahun=null) {
		if( is_null($tahun) ) {
			$tahun = date("Y");
		}
		$ret = array();
		$siswa = $this->getSiswa($id_siswa);
		$tanggungan = ( !empty( $siswa['tagihan_bulanan'] ) ) ? unserialize($siswa['tagihan_bulanan']) : array();
		
		$ret['total_items'] = 0;
		$ret['results'] = array();
		foreach( $tanggungan as $id_jenis_pendapatan ) {
			
				$selectStatement = $this->pdo->select()
					->from('pembayaran')
					->where('id_siswa', '=', $id_siswa, 'AND')
					->where('id_jenis_pendapatan', '=', $id_jenis_pendapatan);
				$stmt = $selectStatement->execute();
				$data = $stmt->fetch();
			
				if( false !== $data ) {
					$ret['total_items']++;
					$ret['results'][] = $data;
				}
		}
		
		return $ret;
		
	}
	
	public function getTanggunganSiswaByMonth($id_siswa, $id_jenis_pendapatan, $bulan, $tahun=null) {
		if( is_null($tahun) ) {
			$tahun = date("Y");
		}
		
		$selectStatement = $this->pdo->select()
			->from('pembayaran')
			->whereMany(array(
					'id_siswa'						=> $id_siswa,
					'id_jenis_pendapatan'	=> $id_jenis_pendapatan,
					'bulan_ke'						=> $bulan,
					'tahun'								=> $tahun
			), '=');
		$stmt = $selectStatement->execute();
		$data = $stmt->fetch();
		
		return $data;
		
	}

}