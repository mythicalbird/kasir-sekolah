<?php

require_once __DIR__ . ("/app.php");

$table = 'users';
$aksi = ( !empty($_GET['aksi']) ) ? $_GET['aksi'] : 'index';


if ( $_SERVER['REQUEST_METHOD'] == "POST" ) {
  
  $id_user = ( !empty($_GET['id']) ) ? $_GET['id'] : 0;

  if ( isset($_POST['simpan']) ) {

    $params = array(
      'id_level'      => $_POST['id_level'],
      'nama'          => $_POST['nama'],
      'email'         => $_POST['email'],
      'username'      => $_POST['username']
    );
		
		// password field
		if( $aksi == 'new' ) {
			$password = ( !empty($_POST['password']) ) ? $_POST['password'] : '';
			$params['password'] = $app->encode_pw($password);
		}

    $result = $app
      ->updateOrInsert( 
        $table, 
        $params,
        $id_user
      );

    $app->add_flash('success', 'Data user berhasil disimpan...');
		$app->redirect( 'user.php' );
    
  } 
	elseif ( isset($_POST['submit-module']) ) {

		
		$modules = $_POST['modul'];

		foreach ($modules as $key => $value) :
		
			$modul = $app->getModule( $key );
			
			if ( false !== $modul ) {
				
				if ( !empty( $value['roles'] ) ) :
				
					$params = array(
						'slug'	=> $value['nama'],
						'roles'	=> serialize( $value['roles'] )
					);
					if ( isset( $value['aktif'] ) ) {
						$params['active'] = $value['aktif'];
					}
				
					$app->updateOrInsert(
						'modules',
						$params,
						$modul['id']
					);
					
				endif;
			
			}
				
		endforeach;
		
		$app->add_flash( 'success', 'Kontrol hak akses berhasil diperbarui...' );
    
	} 
	elseif ( isset($_POST['submit-level']) ) {
		
		$table = 'master';
		$params = array(
			'type'	=> 'user_level',
			'kode'	=> $app->slugify( $_POST['kode'] ),
			'nama'	=> $_POST['nama'],
		);
		$app->updateOrInsert(
			$table,
			$params,
			0
		);
		$app->add_flash( 'success', 'Level baru berhasil ditambah...' );
		
  } 

} else {
	
	$data['levels'] = $app->getMaster('user_level');
	$data['roles'] = $app->getMaster('user_level');

	if ( $aksi == "edit" ) {

		if ( !empty($_GET['id']) ) {
			$data['request'] = $app->getUser($_GET['id']);
		}

	} else {

		$level = ( isset($_GET['level']) ) ? $_GET['level'] : '';
		$data['data'] = $app->getListUser($level);

	}
	
}

try {

	echo $app->load( 
		'default/user_'.$aksi.'.html.twig', 
		$data 
	);
  
} catch (Exception $e) {

  die ('ERROR: ' . $e->getMessage());

}