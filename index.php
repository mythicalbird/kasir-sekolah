<?php

require_once __DIR__ . ("/app.php");

try {

  // load template
  if(isset($_GET['_uri'])) {
    $uri = strtolower($_GET['_uri']);
    $page = (file_exists(dirname(__FILE__) . "/app/" . $uri . ".php")) ? $uri : "404";
  } else {
		$page = "home";
	}
  
  include PATH . "/app/" . $page . ".php";
  
} catch (Exception $e) {
  die ('ERROR: ' . $e->getMessage());
}